
package cz.utb.fai.apac.processor;

import cz.utb.fai.apac.commons.CompileResult;
import cz.utb.fai.apac.commons.ExecutionResult;
import cz.utb.fai.apac.commons.enums.Extension;
import cz.utb.fai.apac.commons.enums.Language;
import cz.utb.fai.apac.commons.spi.Processor;
import cz.utb.fai.apac.commons.spi.ProcessorInfo;
import cz.utb.fai.apac.commons.util.DirFilter;
import cz.utb.fai.apac.commons.util.FileUtil;
import cz.utb.fai.apac.commons.util.GenericExtFilter;
import cz.utb.fai.apac.commons.util.ScoreUtil;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Implementation of {@link Processor} for C++ programming language.
 *
 * @author František Špaček
 */
@ProcessorInfo(language = Language.CPP, version = 1.0)
public class CPPProcessor implements Processor {

  private static final Logger LOGGER = LoggerFactory.getLogger(Processor.class);
  private static final String COMPILER_CPP_COMMAND = "g++ -Wall";
  private static final String COMPILER_CPP_OPTIONAL_ARGS = "-fopenmp";
  private static final String VALGRIND_COMMAND = "valgrind --leak-check=full "
      + "./%s.%s";

  private static final GenericExtFilter SOURCE_FILES_FILTER
      = new GenericExtFilter(new String[]{"cpp", "h", "hxx", "cxx",
    "cp", "cc"});

  private static final double W1 = 0.5;
  private static final double W2 = 0.5;

  @Override
  public boolean isCompilationRequired() {
    return true;
  }

  @Override
  public String compileCommand(File submissionWorkspace) {
    String command = COMPILER_CPP_COMMAND;
    File[] sourcesDirs = submissionWorkspace.listFiles(new DirFilter());

    if (sourcesDirs.length > 0) {
      try {
        FileUtil.getFilesFromDirs(submissionWorkspace, sourcesDirs,
            SOURCE_FILES_FILTER);
      } catch (IOException ex) {
        LOGGER.error("Copy files from dir exception", ex);
      }
    }
    String outputFile = String.format("%s.%s", submissionWorkspace.getName(),
        Extension.BUILD.toString());

    String fileList = getFileList(submissionWorkspace);

    if (fileList.isEmpty()) {
      command = "";
    } else {
      command += String.format(" %s -o %s %s", fileList, outputFile,
          COMPILER_CPP_OPTIONAL_ARGS);
    }

    return command;
  }

  @Override
  public boolean compilationSuccess(CompileResult compileResult) {
    Objects.requireNonNull(compileResult, "Compile result cannot be null");

    boolean success = true;

    if (StringUtils.isNotBlank(compileResult.getOutput())
        && compileResult.getOutput().contains("error")) {
      success &= false;
    }
    return success;
  }

  @Override
  public String executionCommand(File submissionWorkspace) {
    return String.format(VALGRIND_COMMAND, submissionWorkspace.getName(),
        Extension.BUILD.toString());
  }

  @Override
  public boolean executionSuccess(ExecutionResult executionResult) {
    Objects.requireNonNull(executionResult, "Compile result cannot be null");
    boolean success = true;

    if (StringUtils.isNotBlank(executionResult.getOutput())
        && executionResult.getOutput().contains("killed")) {
      success &= false;
    }
    return success;
  }

  @Override
  public int calcCompilationScore(CompileResult compileResult) {
    Objects.requireNonNull(compileResult, "Compile result cannot be null");
    return StringUtils.isNotBlank(compileResult.getOutput())
        ? calcWarnings(compileResult.getOutput()) : 0;
  }

  @Override
  public double calcExecutionScore(ExecutionResult executionResult) {

    String referenceOutput = executionResult.getTestVector()
        .getReferenceOutput();
    double maxPoints = executionResult.getTestVector().getMaxPoints();

    String cleanOutput = getOutputFromValgrindOutput(executionResult
        .getOutput());
    double maxSimilarityPoints = W1 * maxPoints;
    double maxMemoryLeaksPoints = W2 * maxPoints;

    if (referenceOutput != null && !referenceOutput.isEmpty()) {
      double similarity = ScoreUtil.calcOutputSimilarity(cleanOutput,
          referenceOutput);

      if (similarity < (executionResult.getOutputThreshold() / 100.0)) {
        similarity = 0;

      }
      maxSimilarityPoints *= similarity;
    }

    if (containsMemoryLeaks(executionResult.getOutput())) {
      maxMemoryLeaksPoints = 0;
    }
    return maxMemoryLeaksPoints + maxSimilarityPoints;

  }

  private int calcWarnings(String compilerResult) {
    int warnings = 0;
    Scanner scanner = new Scanner(compilerResult);
    while (scanner.hasNextLine()) {
      if (scanner.nextLine().contains("warning")) {
        warnings++;
      }
    }

    return warnings;
  }

  private boolean containsMemoryLeaks(String valgrindOutput) {
    String definitelyLost = StringUtils.substringBetween(valgrindOutput,
        "definitely lost", "bytes");

    if (definitelyLost == null || definitelyLost.isEmpty()) {
      return false;
    }

    int lostBytes = Integer.valueOf(definitelyLost.replaceAll("[^0-9]", ""));
    return lostBytes > 0;
  }

  private String getFileList(File workspace) {
    Set<String> sourceFiles = new HashSet<>(Arrays.asList(workspace
        .list(SOURCE_FILES_FILTER)));

    StringBuilder sb = new StringBuilder();
    sourceFiles.stream().map((s) -> {
      sb.append(s);
      return s;
    }).forEach((_item) -> {
      sb.append(" ");
    });

    return sb.toString();
  }

  public String getOutputFromValgrindOutput(String valgrindOutput) {
    StringBuilder programOutput = new StringBuilder();

    Scanner scanner = new Scanner(valgrindOutput);
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      if (!line.startsWith("==")) {
        programOutput.append(line.replaceFirst("[=]{2}[0-9]{0,4}[=]{2}", "")
            .trim());
        programOutput.append(System.getProperty("line.separator"));
      }
    }
    return programOutput.toString();
  }

}
