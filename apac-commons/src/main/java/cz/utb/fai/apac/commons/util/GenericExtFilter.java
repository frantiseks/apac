/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.commons.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author František Špaček
 */
public class GenericExtFilter implements FilenameFilter {

  private final Set<String> extensions;

  public GenericExtFilter(String[] extensions) {
    this.extensions = new HashSet<>(Arrays.asList(extensions));
  }

  @Override
  public boolean accept(File dir, String name) {
    return extensions.stream()
        .anyMatch((ext) -> (name.toLowerCase().endsWith(ext.toLowerCase())));
  }
}
