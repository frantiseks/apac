/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.commons.enums;

/**
 * @author František Špaček
 */
public enum Language {

  C, CPP, JAVA, CSHARP, PYTHON, CUDA, JAVASCRIPT, PHP, RUBY;

  public static boolean isSupported(String mediaType) {
    boolean supported = false;
    switch (mediaType) {
      case "text/x-csharp":
      case "text/x-csrc":
      case "text/x-java-source":
      case "text/x-php":
      case "text/x-python":
      case "text/x-ruby":
      case "text/x-c++src":
      case "text/javascript":
        supported = true;
        break;
    }

    return supported;
  }
}
