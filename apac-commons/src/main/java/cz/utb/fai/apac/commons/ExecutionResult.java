/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.commons;

/**
 * DTO holds execution result.
 *
 * @author František Špaček
 */
public class ExecutionResult {

  private int outputThreshold;
  private String output;
  private boolean terminated;
  private long duration;
  private double points;
  private TestVector testVector;

  public String getOutput() {
    return output;
  }

  public void setOutput(String output) {
    this.output = output;
  }

  public long getDuration() {
    return duration;
  }

  public void setDuration(long duration) {
    this.duration = duration;
  }

  public boolean isTerminated() {
    return terminated;
  }

  public void setTerminated(boolean terminated) {
    this.terminated = terminated;
  }

  public int getOutputThreshold() {
    return outputThreshold;
  }

  public void setOutputThreshold(int outputThreshold) {
    this.outputThreshold = outputThreshold;
  }

  public double getPoints() {
    return points;
  }

  public void setPoints(double points) {
    this.points = points;
  }

  public TestVector getTestVector() {
    return testVector;
  }

  public void setTestVector(TestVector testVector) {
    this.testVector = testVector;
  }

  public void setTestVector(String refOutput, double maxPoints) {
    this.testVector = new TestVector(refOutput, maxPoints);
  }

  @Override
  public String toString() {
    return "ExecutionResult{" + "outputThreshold=" + outputThreshold
        + ", output=" + output + ", terminated=" + terminated
        + ", duration=" + duration + ", points=" + points
        + ", testVector=" + testVector + '}';
  }

}
