/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.commons.spi;

import cz.utb.fai.apac.commons.CompileResult;
import cz.utb.fai.apac.commons.ExecutionResult;

import java.io.File;

/**
 * Defines submission processor API. Implementations of this interface are used to process submit
 * student's solutions.
 *
 * @author František Špaček
 */
public interface Processor {

  /**
   * Checks if is compilation process required.
   *
   * @return {@code true} if compilation is required,otherwise {@code false}
   */
  public boolean isCompilationRequired();

  /**
   * Builds compile command for provided submission workspace.
   *
   * @param submissionWorkspace submission workspace for processed submission.
   * @return command for compilation, never {@code null}
   */
  public String compileCommand(File submissionWorkspace);

  /**
   * Returns if compilation was successful.
   *
   * @param compileResult compile result
   * @return {@code true} if successful, otherwise {@code false}
   */
  public boolean compilationSuccess(CompileResult compileResult);

  /**
   * Execution command.
   *
   * @param submissionWorkspace
   * @return
   */
  public String executionCommand(File submissionWorkspace);

  /**
   * @param executionResult
   * @return
   */
  public boolean executionSuccess(ExecutionResult executionResult);

  public int calcCompilationScore(CompileResult compileResult);

  public double calcExecutionScore(ExecutionResult executionResult);
}
