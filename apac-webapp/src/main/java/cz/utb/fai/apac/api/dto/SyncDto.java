/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.apac.api.dto;

import java.util.Map;

/**
 * @author František Špaček
 */
public class SyncDto {

  private Map<String, Object> data;

  public Map<String, Object> getData() {
    return data;
  }

  public void setData(Map<String, Object> data) {
    this.data = data;
  }
}
