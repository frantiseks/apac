/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.domain;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author František Špaček
 */
@Entity
@Table(name = "submission_similarities",
    uniqueConstraints = @UniqueConstraint(
        columnNames = {"base_submission_id", "suspected_submission_id"}))
public class SubmissionSimilarity extends BaseEntity {

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "base_submission_id")
  private Submission base;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "suspected_submission_id")
  private Submission suspected;

  @Column(nullable = false)
  private double similarity = 0d;

  public SubmissionSimilarity() {
  }

  public SubmissionSimilarity(Submission base, Submission suspected, double similarity) {
    this.base = base;
    this.suspected = suspected;
    this.similarity = similarity;
  }

  public Submission getBase() {
    return base;
  }

  public void setBase(Submission base) {
    this.base = base;
  }

  public Submission getSuspected() {
    return suspected;
  }

  public void setSuspected(Submission suspected) {
    this.suspected = suspected;
  }

  public double getSimilarity() {
    return similarity;
  }

  public void setSimilarity(double similarity) {
    this.similarity = similarity;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 89 * hash + Objects.hashCode(this.base);
    hash = 89 * hash + Objects.hashCode(this.suspected);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final SubmissionSimilarity other = (SubmissionSimilarity) obj;
    if (!Objects.equals(this.base, other.base)) {
      return false;
    }
    return Objects.equals(this.suspected, other.suspected);
  }

  @Override
  public String toString() {
    return "SubmissionSimilarity{" + "base=" + base + ", suspected=" + suspected
        + ", similarity=" + similarity + '}';
  }
}
