/*
 * Copyright (C) 2015 , František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.service.impl;

import cz.utb.fai.apac.api.dto.PlagiarismSyncDto;
import cz.utb.fai.apac.api.dto.SubmissionSimilarityDto;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.domain.SubmissionSimilarity;
import cz.utb.fai.apac.repository.SubmissionRepository;
import cz.utb.fai.apac.repository.SubmissionSimilarityRepository;
import cz.utb.fai.apac.service.SyncService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Implementation of {@link SyncService}.
 *
 * @author František Špaček
 */
@Service
@Transactional
public class SyncServiceImpl implements SyncService {

  private static final Logger LOG = LoggerFactory.getLogger(SyncServiceImpl.class);
  @Autowired
  private SubmissionRepository submissionRepository;

  @Autowired
  private SubmissionSimilarityRepository submissionSimilarityRepository;

  @Override
  public void syncPlagiarism(List<PlagiarismSyncDto> dtos) {

    for (PlagiarismSyncDto dto : dtos) {
      final String baseSubmissionUuid = dto.getBaseUuid().replaceAll("-", "");
      LOG.info("Processing PlagDetector results for base submission {}", baseSubmissionUuid);
      LOG.info("Suspected similar submission are {}", dto.getSimilarSubmissions());

      //find base submission in storage
      Optional<Submission> opSubmission = submissionRepository.findByUuid(baseSubmissionUuid);

      if (!opSubmission.isPresent()) {
        continue;
      }
      Submission submission = opSubmission.get();
      submission.setPlagiarismChecked(true);
      submission.setSimilarity(dto.getSimilarity());
      //deletes all old suspected 
      try {
        submissionSimilarityRepository.deleteByBase(submission);

      } catch (DataAccessException ex) {
        LOG.error("Error occurs during deleting old suspected", ex);
      }
      final List<SubmissionSimilarityDto> supected = dto.getSimilarSubmissions();

      Set<SubmissionSimilarity> similarities = new LinkedHashSet();
      for (SubmissionSimilarityDto s : supected) {
        final String submissionUuid = s.getUuid().replace("-", "");
        LOG.info("Processing suspected submission {}", submissionUuid);
        Optional<Submission> opSuspected = submissionRepository.findByUuid(submissionUuid);

        if (!opSuspected.isPresent()) {
          continue;
        }
        SubmissionSimilarity subSimilarity = new SubmissionSimilarity();
        subSimilarity.setBase(submission);
        subSimilarity.setSuspected(opSuspected.get());
        subSimilarity.setSimilarity(s.getSimilarity());
        similarities.add(subSimilarity);
      }

      try {
        submissionSimilarityRepository.save(similarities);
        submission.setSimilarSubmissions(similarities);
        submissionRepository.save(submission);
      } catch (DataAccessException ex) {
        LOG.error("Unable to save plagiarism check results", ex);
      }
    }
  }
}
