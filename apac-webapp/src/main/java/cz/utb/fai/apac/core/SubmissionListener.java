/*
 * Copyright (C) 2015 , František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.core;

import cz.utb.fai.apac.commons.CompileResult;
import cz.utb.fai.apac.commons.ExecutionResult;
import cz.utb.fai.apac.commons.spi.Processor;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Execution;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.service.AssignmentService;
import cz.utb.fai.apac.service.SubmissionService;
import cz.utb.fai.apac.service.VectorService;
import cz.utb.fai.apac.util.WorkspaceUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 * @author František Špaček
 */
@Component
@Transactional
public class SubmissionListener implements MessageListener {

  private static final Logger LOG = LoggerFactory.getLogger(SubmissionListener.class);

  @Autowired
  private ApplicationContext context;

  @Autowired
  private SubmissionMonitor monitor;

  @Autowired
  private ProcessorFactory processorFactory;

  @Autowired
  private AssignmentService assignmentService;

  @Autowired
  private SubmissionService SubmissionService;

  @Autowired
  private VectorService vectorService;

  @Value("${apac.temp}")
  private String basePath;

  @Override
  public void onMessage(Message msg) {
    try {
      boolean validMsgInstance = msg instanceof ObjectMessage;

      //if instance is wrong end of processing
      if (!validMsgInstance) {
        LOG.error("Wrong instance of message", msg);
        return;
      }
      ObjectMessage objMsg = (ObjectMessage) msg;
      if (objMsg == null) {
        return;
      }

      SubmissionInfo submissionInfo = (SubmissionInfo) objMsg.getObject();
      String uuid = submissionInfo.getUuid();
      monitor.sendInfo(uuid, "Processing submission...");

      Executor executor = (Executor) context.getBean("container-executor");
      //create executor instance

      Optional<Processor> opProcessor = processorFactory.getInstance(submissionInfo.getLanguage());

      if (!opProcessor.isPresent()) {
        monitor.sendError(uuid, "Incompatible submission");
        monitor.sendEnd(uuid);
        WorkspaceUtil.deleteWorkspace(basePath, uuid);
        return;
      }
      Processor processor = opProcessor.get();

      Assignment assignment = assignmentService.get(submissionInfo.getAssignmentId());
      Submission submission = SubmissionService.getOrNew(uuid);
      submission.setAssignment(assignment);
      submission.setUuid(uuid);
      submission.setOwner(submissionInfo.getOwner());
      submission.setReference(submissionInfo.isReference());
      SubmissionService.createOrUpdate(submission);

      List<Vector> vectors = assignment.getVectors();
      double totalPoints = 0;
      double maxPoints = calcMaxPoints(vectors);

      boolean executable = true;
      if (processor.isCompilationRequired()) {
        monitor.sendInfo(uuid, "Compiling submission...");
        CompileResult compileResult = compile(executor, submission, processor);

        if (compileResult.isFailed()) {
          monitor.sendError(uuid, "Compilation failed");
          executable = false;
        } else {
          if (!submissionInfo.isReference()
              && processor.calcCompilationScore(compileResult)
              <= assignment.getAllowedCompileWarnings()) {
            totalPoints += assignment.getCompilationWeight() * maxPoints;
          }
          monitor.sendInfo(uuid, "Compilation successful");
        }

        SubmissionService.createOrUpdate(submission);
      }

      if (executable) {
        monitor.sendInfo(uuid, "Testing submission...");
        double totalExecutionPoints = 0;
        List<Execution> executions = new ArrayList<>();
        for (Vector vector : vectors) {
          Execution execution = executeAndGrade(executor, uuid, processor,
              submission, vector, submissionInfo.isReference());

          totalExecutionPoints += execution.getScore();

          if (execution.getTerminated()) {
            monitor.sendError(uuid, "Execution terminated");
          }
          executions.add(execution);
        }

        SubmissionService.updateExecutions(submission, executions);

        if (!submissionInfo.isReference()) {
          totalPoints += assignment.getExecutionWeight() * totalExecutionPoints;
        }

        Optional<Execution> firstSuccessExecution = executions.stream()
            .filter(e -> e.getTerminated() == false)
            .findFirst();

        if (submissionInfo.isReference()) {
          assignment.setCreatedReference(firstSuccessExecution.isPresent());
          assignment.setVisible(firstSuccessExecution.isPresent());
          assignmentService.internalUpdate(assignment);
        }
      }
      submission.setTotalPoints(totalPoints);
      SubmissionService.createOrUpdate(submission);

      executor.cleanUp(uuid);
      monitor.sendInfo(uuid, "Testing completed");
      monitor.sendEnd(uuid);

    } catch (JMSException ex) {
      LOG.error("Exception on submission consumer", ex);
    }
  }

  private Execution executeAndGrade(final Executor executor, String uuid, Processor processor,
      Submission submission, Vector vector, boolean reference) {

    Execution execution = new Execution();

    ExecutionResult executionResult = executor.execute(uuid, processor, vector,
        submission.getAssignment().getMaxRuntime());

    boolean terminated = !processor.executionSuccess(executionResult);

    if (reference && !terminated) {
      vectorService.updateRefOutput(vector, executionResult.getOutput());
    }

    if (!reference) {
      if (terminated) {
        execution.setScore(0d);
      } else {
        execution.setScore(processor.calcExecutionScore(executionResult));
      }
    }

    if (executionResult.getOutput() != null) {
      execution.setLog(executionResult.getOutput());
    }
    execution.setVector(vector);
    execution.setTerminated(terminated);
    execution.setSubmission(submission);
    return execution;
  }

  private CompileResult compile(final Executor executor, Submission submission,
      final Processor processor) {
    CompileResult compileResult = executor.compile(submission.getUuid(), processor);

    submission.setCompileOutput(compileResult.getOutput());
    submission.setCompileFailed(compileResult.isFailed());
    submission.setRunnable(!compileResult.isFailed());
    SubmissionService.createOrUpdate(submission);

    return compileResult;
  }

  private double calcMaxPoints(List<Vector> vectors) {
    double maxPoints = 0;
    return vectors
        .stream().map((v) -> v.getMaxPoints())
        .reduce(maxPoints, (accumulator, _item) -> accumulator + _item);
  }
}
