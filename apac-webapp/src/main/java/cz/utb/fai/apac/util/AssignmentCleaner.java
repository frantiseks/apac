/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.util;

import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.repository.AssignmentRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author František Špaček
 */
@Component
public class AssignmentCleaner {

  private static final Logger LOG = LoggerFactory.getLogger(AssignmentCleaner.class);
  @Autowired
  private AssignmentRepository assignmentRepository;

  @Scheduled(cron = "0 0 * * * *")
  public void clean() {
    LOG.info("Assignment cleaner starts");
    List<Assignment> assignments = assignmentRepository.findAllToHide();
    assignments.stream()
        .forEach(a -> {
          a.setVisible(false);
        });
    assignmentRepository.save(assignments);

    assignments = assignmentRepository.findAllToShow();
    assignments.stream()
        .forEach(a -> {
          a.setVisible(true);
        });
    assignmentRepository.save(assignments);

    LOG.info("Assignment cleaner ends");
  }
}
