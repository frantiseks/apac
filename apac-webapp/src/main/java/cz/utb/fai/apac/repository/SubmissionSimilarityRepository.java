/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.repository;

import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.domain.SubmissionSimilarity;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 *
 * @author František Špaček
 */
@Repository
public interface SubmissionSimilarityRepository extends CrudRepository<SubmissionSimilarity, Long> {

  public Optional<SubmissionSimilarity> findOneByBaseAndSuspected(Submission base, 
      Submission suspected);

  @Modifying
  @Transactional
  @Query("DELETE FROM SubmissionSimilarity WHERE base = ?1")
  public void deleteByBase(Submission base);

}
