/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import cz.utb.fai.apac.api.dto.ArgumentDto;
import cz.utb.fai.apac.api.dto.VectorDto;
import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.enums.ArgumentType;
import cz.utb.fai.apac.exception.InvalidVectorException;
import cz.utb.fai.apac.service.VectorService;
import cz.utb.fai.apac.util.ArgumentGenerator;
import cz.utb.fai.apac.util.DirUtil;
import cz.utb.fai.apac.util.VectorUtil;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author František Špaček
 */
@RestController
@Api(value = "Vectors", description = "Endpoint for vectors management")
@RequestMapping("/api/vectors")
public class VectorResource extends CrudResource<VectorDto, Long> {
  
  @Value("${apac.uploads}")
  private String uploadPath;
  
  @Autowired
  private VectorService vectorService;
  
  @Override
  @ApiOperation(value = "Get vector")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Vector found"),
    @ApiResponse(code = 404, message = "Vector not found")
  })
  @RequestMapping(value = "{id}", method = RequestMethod.GET)
  public VectorDto get(
      @ApiParam(required = true, name = "id", value = "Vector id")
      @PathVariable("id") Long id) {
    return VectorDto.fromEntity(vectorService.get(id));
  }
  
  @Override
  @ApiOperation(value = "Update vector")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Vector updated"),
    @ApiResponse(code = 400, message = "Invalid vector json"),
    @ApiResponse(code = 404, message = "Vector not found")
  })
  @RequestMapping(value = "{id}", method = RequestMethod.PUT)
  public VectorDto update(
      @ApiParam(required = true, name = "id", value = "Vector id")
      @PathVariable("id") Long id,
      @Valid @RequestBody VectorDto dto) {
    if (!VectorUtil.validate(dto)) {
      throw new InvalidVectorException("Vector is not valid");
    }
    dto.setId(id);
    Vector vector = VectorDto.toEntity(dto);
    vector = vectorService.update(vector);
    return VectorDto.fromEntity(vector);
  }
  
  @Override
  @ApiOperation(value = "Delete vector")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Vector found and deleted"),
    @ApiResponse(code = 404, message = "Vector not found")
  })
  @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
  public void delete(
      @ApiParam(required = true, name = "id", value = "Vector id")
      @PathVariable("id") Long id) {
    vectorService.delete(id);
    
  }
  
  @ApiOperation(value = "Get vector arguments")
  @ApiResponses({
    @ApiResponse(code = 200, message = "List of arguments"),
    @ApiResponse(code = 404, message = "Vector not found")
  })
  @RequestMapping(value = "/{id}/arguments", method = RequestMethod.GET)
  public List<ArgumentDto> getArguments(
      @ApiParam(required = true, name = "id", value = "Vector id")
      @PathVariable("id") Long id) {
    List<Argument> args = vectorService.get(id).getArguments();
    return args.stream()
        .map(ArgumentDto::fromEntity)
        .collect(Collectors.toList());
  }
  
  @ApiOperation(value = "Upload vector file")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Vector file successful uploaded"),
    @ApiResponse(code = 404, message = "Vector not found")
  })
  @RequestMapping(value = "{id}/upload", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public void uploadFile(
      @ApiParam(required = true, name = "id", value = "Vector id")
      @PathVariable("id") Long id,
      @NotNull HttpEntity<byte[]> file,
      @ApiParam(required = true, name = "filename",
          value = "Filename of vector file")
      @NotNull @Size(min = 1) @RequestParam("filename") String filename)
      throws IOException {
    
    if (!file.hasBody()) {
      throw new IllegalArgumentException("Request body cannot be empty");
    }
    
    File uploadedFile = new File(DirUtil.fixPath(uploadPath) + filename);
    FileUtils.writeByteArrayToFile(uploadedFile, file.getBody());
    
    vectorService.updateFile(id, uploadedFile);
  }
  
  @ApiOperation(value = "Generate argument")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Argument generated")
  })
  @RequestMapping(value = "gen_arguments", method = RequestMethod.POST)
  public ArgumentDto generateArgument(
      @ApiParam(required = true, name = "type", value = "ArgumentType")
      @RequestParam("type") ArgumentType type,
      @ApiParam(required = true, name = "name", value = "Argument name")
      @RequestParam("name") String name,
      @ApiParam(required = true, name = "length", value = "Argument value length")
      @RequestParam("length") int length) {
    return ArgumentGenerator.generate(type, name, length);
  }
  
}
