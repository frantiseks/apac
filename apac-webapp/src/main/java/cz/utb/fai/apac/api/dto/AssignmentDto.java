/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api.dto;

import com.wordnik.swagger.annotations.ApiModel;

import cz.utb.fai.apac.domain.Assignment;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author František Špaček
 */
@ApiModel
public class AssignmentDto extends BaseDto {

  @NotNull
  @Size(min = 1)
  private String language;

  @Min(50)
  @Max(100)
  private int threshold = 60;

  @Min(1)
  @Max(30)
  private int maxRuntime = 10;

  @Min(1)
  @Max(5)
  private int maxSize = 1;

  @NotNull
  @Size(min = 3)
  private String name;

  private String description;

  private int allowedCompileWarnings = 0;
  private double compilationWeight = 0.5;
  private double executionWeight = 0.5;

  @NotNull
  private Long validFrom = System.currentTimeMillis();

  @NotNull
  private Long validTo = Date.from(LocalDateTime
      .ofInstant(Instant.ofEpochMilli(validFrom), ZoneId.systemDefault())
      .plusWeeks(4)
      .atZone(ZoneId.systemDefault())
      .toInstant())
      .getTime();

  public static AssignmentDto fromEntity(Assignment entity) {
    AssignmentDto dto = new AssignmentDto();
    dto.setId(entity.getId());
    dto.setLanguage(entity.getLang());
    dto.setMaxRuntime(entity.getMaxRuntime());
    dto.setMaxSize(entity.getMaxSize());
    dto.setName(entity.getName());
    dto.setThreshold(entity.getThreshold());
    dto.setDescription(entity.getDescription());
    dto.setAllowedCompileWarnings(entity.getAllowedCompileWarnings());
    dto.setCompilationWeight(entity.getCompilationWeight());
    dto.setExecutionWeight(entity.getExecutionWeight());

    if (entity.getValidFrom() != null) {
      Instant validFrom = entity.getValidFrom().atZone(ZoneId.systemDefault()).toInstant();
      dto.setValidFrom(Date.from(validFrom).getTime());
    }

    if (entity.getValidFrom() != null) {
      Instant validTo = entity.getValidTo().atZone(ZoneId.systemDefault()).toInstant();
      dto.setValidTo(Date.from(validTo).getTime());
    }

    return dto;
  }

  public static Assignment toEntity(AssignmentDto dto) {
    Assignment entity = new Assignment();
    entity.setId(dto.getId());
    entity.setLang(dto.getLanguage());
    entity.setMaxRuntime(dto.getMaxRuntime());
    entity.setMaxSize(dto.getMaxSize());
    entity.setName(dto.getName());
    entity.setThreshold(dto.getThreshold());
    entity.setDescription(dto.getDescription());
    entity.setCompilationWeight(dto.getCompilationWeight());
    entity.setExecutionWeight(dto.getExecutionWeight());
    entity.setAllowedCompileWarnings(dto.getAllowedCompileWarnings());

    LocalDateTime validFrom = LocalDateTime
        .ofInstant(Instant.ofEpochMilli(dto.getValidFrom()), ZoneId.systemDefault());
    LocalDateTime validTo = LocalDateTime
        .ofInstant(Instant.ofEpochMilli(dto.getValidTo()), ZoneId.systemDefault());

    entity.setValidFrom(validFrom);
    entity.setValidTo(validTo);

    return entity;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public int getThreshold() {
    return threshold;
  }

  public void setThreshold(int threshold) {
    this.threshold = threshold;
  }

  public int getMaxRuntime() {
    return maxRuntime;
  }

  public void setMaxRuntime(int maxRuntime) {
    this.maxRuntime = maxRuntime;
  }

  public int getMaxSize() {
    return maxSize;
  }

  public void setMaxSize(int maxSize) {
    this.maxSize = maxSize;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getAllowedCompileWarnings() {
    return allowedCompileWarnings;
  }

  public void setAllowedCompileWarnings(int allowedCompileWarnings) {
    this.allowedCompileWarnings = allowedCompileWarnings;
  }

  public double getCompilationWeight() {
    return compilationWeight;
  }

  public void setCompilationWeight(double compilationWeight) {
    this.compilationWeight = compilationWeight;
  }

  public double getExecutionWeight() {
    return executionWeight;
  }

  public void setExecutionWeight(double executionWeight) {
    this.executionWeight = executionWeight;
  }

  public Long getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Long validFrom) {
    this.validFrom = validFrom;
  }

  public Long getValidTo() {
    return validTo;
  }

  public void setValidTo(Long validTo) {
    this.validTo = validTo;
  }

  @Override
  public String toString() {
    return "AssignmentDTO{" + "id=" + super.getId() + ", language="
        + language + ", threshold=" + threshold + ", maxRuntime="
        + maxRuntime + ", maxSize=" + maxSize + ", name=" + name
        + ", description=" + description + '}';
  }

}
