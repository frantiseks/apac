/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import cz.utb.fai.apac.api.dto.AssignmentDto;
import cz.utb.fai.apac.api.dto.SubmissionDto;
import cz.utb.fai.apac.api.dto.VectorDto;
import cz.utb.fai.apac.api.dto.filter.AssignmentFilter;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.exception.InvalidVectorException;
import cz.utb.fai.apac.service.AssignmentService;
import cz.utb.fai.apac.service.SubmissionService;
import cz.utb.fai.apac.util.UploadUtil;
import cz.utb.fai.apac.util.VectorUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author František Špaček
 */
@RestController
@Api(value = "Assigments", description = "Endpoint for assigment management")
@RequestMapping("/api/assignments")
public class AssignmentResource extends CrudResource<AssignmentDto, Long> {

  @Autowired
  private SubmissionService submissionService;

  @Autowired
  private AssignmentService assignmentService;

  @Value("${apac.uploads}")
  private String uploadPath;

  @Override
  @ApiOperation(value = "Define new assignment")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Assigment created"),
    @ApiResponse(code = 400, message = "Invalid request body")
  })
  @RequestMapping(method = RequestMethod.POST)
  public AssignmentDto create(@Valid @RequestBody AssignmentDto dto) {
    //convert to entity
    Assignment entity = AssignmentDto.toEntity(dto);
    assignmentService.create(entity);

    //convert to dto
    return AssignmentDto.fromEntity(entity);
  }

  @Override
  @ApiOperation(value = "Get assignment details")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Assigment found"),
    @ApiResponse(code = 404, message = "Assigment not found")
  })
  @RequestMapping(value = "{id}", method = RequestMethod.GET)
  public AssignmentDto get(
      @ApiParam(required = true, name = "id", value = "Assignment id")
      @PathVariable("id") @Min(1) Long id) {
    return AssignmentDto.fromEntity(assignmentService.get(id));
  }

  @Override
  @ApiOperation(value = "Update assignment details")
  @ApiResponses({
    @ApiResponse(code = 404, message = "Assigment not found"),
    @ApiResponse(code = 400, message = "Invalid request body"),})
  @RequestMapping(value = "{id}", method = RequestMethod.PUT)
  public AssignmentDto update(
      @ApiParam(required = true, name = "id", value = "Assignment id")
      @PathVariable("id") @Min(1) Long id,
      @Valid @RequestBody AssignmentDto dto) {
    dto.setId(id);
    Assignment entity = AssignmentDto.toEntity(dto);
    entity = assignmentService.update(entity);
    return AssignmentDto.fromEntity(entity);
  }

  @Override
  @ApiOperation(value = "Delete assignment")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Assigment deleted"),
    @ApiResponse(code = 404, message = "Assigment not found")
  })
  @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
  public void delete(
      @ApiParam(required = true, name = "id", value = "Assignment id")
      @PathVariable("id") @Min(1) Long id) {
    assignmentService.delete(id);
  }

  @ApiOperation(value = "Filter assignments")
  @ApiResponses({
    @ApiResponse(code = 200, message = "List of filtered assignments"),
    @ApiResponse(code = 400, message = "Invalid filter json")
  })
  @RequestMapping(value = "/filter", method = RequestMethod.POST)
  public List<AssignmentDto> getAll(
      @RequestBody(required = false) AssignmentFilter filter) {
    List<Assignment> assignments = assignmentService.getAll(filter);
    return assignments.stream()
        .map(AssignmentDto::fromEntity)
        .collect(Collectors.toList());
  }

  @ApiOperation(value = "Upload reference submission")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Submission successful uploaded"),
    @ApiResponse(code = 400, message = "Invalid submission file or "
        + "submission file is not supported")
  })
  @RequestMapping(value = "{id}/upload", method = {RequestMethod.POST},
      consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public SubmissionDto uploadReference(
      @ApiParam(required = true, name = "id", value = "Assignment id")
      @PathVariable("id") long id,
      HttpEntity<byte[]> file,
      @ApiParam(required = false, name = "filename",
          value = "Name of selected file")
      @NotNull @Size(min = 1) @RequestParam(value = "filename",
          required = false, defaultValue = "") String filename)
      throws IOException {
    if(!file.hasBody()){
      throw new IllegalArgumentException("Request body cannot be empty");
    }
    //validate mediaType and copy to uploadPath
    File uploadedFile = UploadUtil
        .processUpload(file.getBody(), uploadPath, filename);

    //process submission
    String uuid = submissionService.processSubmission(id, uploadedFile, true);
    return new SubmissionDto(uuid);
  }

  @ApiOperation(value = "Add test vector to assignment")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Vector added to assignment"),
    @ApiResponse(code = 400, message = "Invalid vector json")
  })
  @RequestMapping(value = "{id}/vectors", method = RequestMethod.POST)
  public VectorDto addTestVector(
      @ApiParam(required = true, name = "id", value = "Assignment id")
      @PathVariable("id") Long id,
      @Valid @RequestBody VectorDto dto) throws IOException {

    if (!VectorUtil.validate(dto)) {
      throw new InvalidVectorException("Vector is not valid");
    }

    Vector entity = VectorDto.toEntity(dto);
    entity = assignmentService.addTestVector(id, entity);
    return VectorDto.fromEntity(entity);
  }

  @ApiOperation(value = "Get test vectors for assignment")
  @ApiResponses({
    @ApiResponse(code = 200, message = "List of vectors"),
    @ApiResponse(code = 404, message = "Assignment not found")
  })
  @RequestMapping(value = "{id}/vectors", method = RequestMethod.GET)
  public List<VectorDto> getVectors(
      @ApiParam(required = true, name = "id", value = "Assignment id")
      @PathVariable("id") Long id) {
    List<Vector> vectors = assignmentService.get(id).getVectors();

    return vectors.stream()
        .map(VectorDto::fromEntity)
        .collect(Collectors.toList());
  }

  @ApiOperation(value = "Upload student's submission for assignment")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Submission uploaded"),
    @ApiResponse(code = 400, message = "Invalid submission file"),
    @ApiResponse(code = 404, message = "Assignment not found"),
    @ApiResponse(code = 417, message = "Assignment processing is incomplete")
  })
  @RequestMapping(value = "{id}/submissions", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public SubmissionDto createSubmission(
      @ApiParam(required = true, name = "id", value = "Assignment id")
      @PathVariable("id") Long id,
      @NotNull HttpEntity<byte[]> file,
      @NotNull @Size(min = 1)
      @ApiParam(required = true, name = "filename",
          value = "Name of selected file")
      @RequestParam(value = "filename",
          required = false, defaultValue = "") String filename,
      @ApiParam(required = true, name = "owner", value = "Owner of selected"
          + " file")
      @RequestParam(value = "owner",
          required = false, defaultValue = "") String owner)
      throws IOException {
    //validate mediaType and copy to uploadPath
    File uploadedFile = UploadUtil
        .processUpload(file.getBody(), uploadPath, filename);

    //process submission
    String uuid = owner.isEmpty()
        ? submissionService.processSubmission(id, uploadedFile, false)
        : submissionService.processSubmission(id, uploadedFile, owner);
    return new SubmissionDto(uuid);
  }

  @ApiOperation(value = "Get submissions for assignment")
  @ApiResponses({
    @ApiResponse(code = 200, message = "List of submissions"),
    @ApiResponse(code = 404, message = "Assignment not found"),})
  @RequestMapping(value = "{id}/submissions", method = RequestMethod.GET)
  public List<SubmissionDto> getSubmissions(
      @ApiParam(required = true, name = "id", value = "Assignment id")
      @PathVariable("id") Long id,
      @ApiParam(required = false, name = "reference",
          value = "Filter only reference submissions")
      @RequestParam(value = "reference", required = false) boolean reference) {
    List<Submission> submissions = submissionService
        .getAllByAssignmentId(id, reference);
    return submissions.stream()
        .map(SubmissionDto::fromEntity)
        .collect(Collectors.toList());
  }
}
