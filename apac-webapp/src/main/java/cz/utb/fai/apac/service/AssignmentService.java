/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.service;

import cz.utb.fai.apac.api.dto.filter.AssignmentFilter;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Vector;

import java.util.List;

/**
 *
 * @author František Špaček
 */
public interface AssignmentService {

  /**
   *
   * @param id
   * @param vector
   * @return
   */
  public Vector addTestVector(long id, Vector vector);

  /**
   *
   * @param entity
   * @return
   */
  public Assignment create(Assignment entity);

  /**
   *
   * @param id
   */
  public void delete(long id);

  /**
   *
   * @param id
   * @return
   */
  public Assignment get(long id);

  /**
   *
   * @param filter
   * @return
   */
  public List<Assignment> getAll(AssignmentFilter filter);

  /**
   *
   * @param entity
   * @return
   */
  public Assignment internalUpdate(Assignment entity);

  /**
   *
   * @param id
   */
  public void reRunAssignment(long id);

  /**
   *
   * @param entity
   * @return
   */
  public Assignment update(Assignment entity);

}
