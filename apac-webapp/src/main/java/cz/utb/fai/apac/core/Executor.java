/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.core;

import cz.utb.fai.apac.commons.CompileResult;
import cz.utb.fai.apac.commons.ExecutionResult;
import cz.utb.fai.apac.commons.spi.Processor;
import cz.utb.fai.apac.domain.Vector;

/**
 * @author František Špaček
 */
public interface Executor {

  /**
   * @param workspaceUUID
   * @param processor
   * @return
   */
  public CompileResult compile(String workspaceUUID, Processor processor);

  /**
   * @param workspaceUUID
   * @param processor
   * @param vector
   * @param maxRuntime
   * @return
   */
  public ExecutionResult execute(String workspaceUUID, Processor processor,
      Vector vector, int maxRuntime);

  /**
   * @param workspaceUUID
   */
  public void cleanUp(String workspaceUUID);
}
