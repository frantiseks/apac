/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.core;

import cz.utb.fai.apac.domain.Assignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Session;

/**
 * @author František Špaček
 */
@Component
public class SubmissionProducer {

  @Value("${apac.jms.destination}")
  private String jmsDestination;

  @Autowired
  private JmsTemplate jmsTemplate;

  public void create(Assignment assignment, String uuid, boolean reference) {
    create(assignment, uuid, reference, "");
  }

  public void create(Assignment assignment, String uuid, boolean reference, String owner) {
    SubmissionInfo submissionInfo = new SubmissionInfo(assignment, uuid, reference);
    submissionInfo.setOwner(owner);
    jmsTemplate.send(jmsDestination, (Session sn) -> sn.createObjectMessage(submissionInfo));
  }
}
