/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api.dto;

import cz.utb.fai.apac.domain.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author František Špaček
 */
public class VectorDto extends BaseDto {

  private Long assignmentId;
  private String output;
  private double points = 0;
  private List<ArgumentDto> arguments = new ArrayList<>();

  public static VectorDto fromEntity(Vector entity) {
    VectorDto dto = new VectorDto();
    dto.setId(entity.getId());
    dto.setOutput(entity.getRefOutput());
    dto.setPoints(entity.getMaxPoints());

    dto.setAssignmentId(entity.getAssignment().getId());

    if (Objects.nonNull(entity.getArguments())) {
      dto.setArguments(entity.getArguments()
          .stream().map(ArgumentDto::fromEntity)
          .collect(Collectors.toList()));
    }
    return dto;
  }

  public static Vector toEntity(VectorDto dto) {
    Vector entity = new Vector();

    if (Objects.nonNull(dto.getId())) {
      entity.setId(dto.getId());
    }
    entity.setMaxPoints(dto.getPoints());

    if (Objects.nonNull(dto.getArguments())) {
      entity.setArguments(dto.getArguments()
          .stream().map(ArgumentDto::toEntity)
          .collect(Collectors.toList()));
    }
    return entity;
  }

  public String getOutput() {
    return output;
  }

  public void setOutput(String output) {
    this.output = output;
  }

  public double getPoints() {
    return points;
  }

  public void setPoints(double points) {
    this.points = points;
  }

  public List<ArgumentDto> getArguments() {
    return arguments;
  }

  public void setArguments(List<ArgumentDto> arguments) {
    this.arguments = arguments;
  }

  public Long getAssignmentId() {
    return assignmentId;
  }

  public void setAssignmentId(Long assignmentId) {
    this.assignmentId = assignmentId;
  }

  @Override
  public String toString() {
    return "VectorDTO{" + "id=" + super.getId() + ", output=" + output
        + ", points=" + points + ", arguments=" + arguments + '}';
  }

}
