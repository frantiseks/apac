/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.apac.api.dto.filter;

import cz.utb.fai.apac.api.dto.BaseDto;

/**
 * @author František Špaček
 */
public class SubmissionFilter extends BaseDto {

  private Long from;
  private Long to;
  private boolean visible = true;

  public Long getFrom() {
    return from;
  }

  public void setFrom(Long from) {
    this.from = from;
  }

  public Long getTo() {
    return to;
  }

  public void setTo(Long to) {
    this.to = to;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  @Override
  public String toString() {
    return "SubmissionFilter{" + "from=" + from + ", to=" + to
        + ", visible=" + visible + '}';
  }

}
