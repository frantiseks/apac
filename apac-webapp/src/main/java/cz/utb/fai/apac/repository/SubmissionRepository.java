/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.repository;

import cz.utb.fai.apac.domain.Submission;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @author František Špaček
 */
@Repository
public interface SubmissionRepository extends CrudRepository<Submission, Long>, JpaSpecificationExecutor<Submission> {

  public Optional<Submission> findByUuid(String uuid);

  public List<Submission> findByAssignmentId(Long assignmentId);

  @Query("SELECT s FROM Submission s WHERE assignment.id = :assignmentId AND s.runnable = true "
      + "AND s.visible = true AND s.compileFailed = false AND s.timestamp BETWEEN :from AND :to")
  public List<Submission> findAllCurrent(@Param("assignmentId") Long assignmentId,
      @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

  @Query("SELECT s FROM Submission s WHERE timestamp BETWEEN :from AND :to")
  public List<Submission> findAllBetween(@Param("from") LocalDateTime from,
      @Param("to") LocalDateTime to);

  @Query("SELECT s FROM Submission s WHERE assignment.id = ?1 AND visible = true AND s.reference = ?2")
  public List<Submission> findAllSubmissions(Long assignmentId, boolean reference);

  @Modifying
  @Transactional
  @Query("UPDATE Submission s SET s.similarity = ?2, s.plagiarismChecked = true WHERE s.uuid = ?1")
  public void setSimilarity(String uuid, Double similarity);

  @Query("SELECT s FROM Submission s JOIN s.assignment a "
      + "WHERE a.validFrom <= ?1 AND a.validTo >= ?1 "
      + "AND s.compileFailed = false AND s.runnable = true "
      + "AND s.reference = false AND s.plagiarismChecked = false AND owner <> '' "
      + "ORDER BY s.timestamp")
  public List<Submission> findAllForPlagiarismCheck(LocalDateTime now);
}
