/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.service.impl;

import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.enums.ArgumentType;
import cz.utb.fai.apac.exception.ResourceNotFoundException;
import cz.utb.fai.apac.repository.ArgumentRepository;
import cz.utb.fai.apac.repository.VectorRepository;
import cz.utb.fai.apac.service.AssignmentService;
import cz.utb.fai.apac.service.VectorService;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * @author František Špaček
 */
@Service
@Transactional
public class VectorServiceImpl implements VectorService {

  private static final Logger LOG = LoggerFactory.getLogger(VectorServiceImpl.class);

  @Autowired
  private VectorRepository vectorRepository;

  @Autowired
  private ArgumentRepository argumentRepository;

  @Autowired
  private AssignmentService assignmentService;

  @Value("${apac.references}")
  private String referenceBasePath;

  @Override
  public Vector get(long id) {
    Optional<Vector> opVector = vectorRepository.findById(id);
    return opVector.orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public Vector update(Vector entity) {
    Vector vector = get(entity.getId());
    argumentRepository.deleteByVector(vector);
    entity.setAssignment(vector.getAssignment());

    Optional<Argument> opVectorFile = vector.getArguments()
        .stream()
        .filter(a -> a.getType().equals(ArgumentType.INPUTFILE)
            || a.getType().equals(ArgumentType.STDINFILE))
        .findFirst();

    if (opVectorFile.isPresent()) {
      entity.setFilename(opVectorFile.get().getValue());
    }

    entity.setRefOutput(vector.getRefOutput());
    entity.getArguments().stream().forEach((arg) -> {
      arg.setVector(vector);
    });

    argumentRepository.save(entity.getArguments());
    entity = vectorRepository.save(entity);
    assignmentService.reRunAssignment(vector.getAssignment().getId());

    return entity;
  }

  @Override
  public void updateRefOutput(Vector entity, String output) {
    entity.setRefOutput(output);
    vectorRepository.save(entity);
  }

  @Override
  public void delete(long id) {
    Vector vector = get(id);
    vector.getArguments().clear();
    vectorRepository.save(vector);

    argumentRepository.deleteByVector(vector);
    vectorRepository.delete(id);
  }

  @Override
  public void updateFile(long id, File uploadedFile) {
    Vector vector = get(id);
    try {
      File referenceFolder = new File(referenceBasePath + File.separator + id);

      if (!referenceFolder.exists()) {
        referenceFolder.mkdirs();
      }

      //copy file to assignments references dir
      FileUtils.copyFileToDirectory(uploadedFile, referenceFolder);
      vector.setFilename(uploadedFile.getName());

    } catch (IOException ex) {
      LOG.error("Error during processing file for vector", ex);
    }

    vectorRepository.save(vector);
    assignmentService.reRunAssignment(vector.getAssignment().getId());

    //delete processed file
    FileUtils.deleteQuietly(uploadedFile);
  }
}
