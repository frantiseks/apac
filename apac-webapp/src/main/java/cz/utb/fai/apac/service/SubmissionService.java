/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.service;

import cz.utb.fai.apac.api.dto.filter.SubmissionFilter;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Execution;
import cz.utb.fai.apac.domain.Submission;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Defines methods over submission processing.
 *
 * @author František Špaček
 */
public interface SubmissionService {

  /**
   *
   * @param entity
   * @return
   */
  public Submission createOrUpdate(Submission entity);

  /**
   *
   * @param uuid
   */
  public void delete(String uuid);

  /**
   *
   * @param uuid
   * @return
   */
  public Submission get(String uuid);

  /**
   *
   * @param filter
   * @return
   */
  public List<Submission> getAll(SubmissionFilter filter);

  /**
   *
   * @param assignmentId
   * @param reference
   * @return
   */
  public List<Submission> getAllByAssignmentId(Long assignmentId, boolean reference);

  /**
   *
   * @param assignment
   * @return
   */
  public List<Submission> getAllCurrent(Assignment assignment);

  /**
   *
   * @param uuid
   * @return
   */
  public Submission getOrNew(String uuid);

  /**
   *
   * @param assignmentId
   * @param submissionFile
   * @param owner
   * @return
   * @throws IOException
   */
  public String processSubmission(long assignmentId, File submissionFile, String owner)
      throws IOException;

  /**
   *
   * @param assignmentId
   * @param submissionFile
   * @param reference
   * @return
   * @throws IOException
   */
  public String processSubmission(long assignmentId, File submissionFile, boolean reference)
      throws IOException;

  /**
   *
   * @param assignmentId
   * @param submissionFile
   * @param uuid
   * @param reference
   * @param owner
   * @return
   * @throws IOException
   */
  public String processSubmission(long assignmentId, File submissionFile, String uuid,
      boolean reference, String owner) throws IOException;

  /**
   *
   * @param uuid
   * @param submissionFile
   * @return
   * @throws IOException
   */
  public String processSubmission(String uuid, File submissionFile) throws IOException;

  /**
   *
   * @param assignment
   * @throws IOException
   */
  public void reRunReferenceSubmission(Assignment assignment) throws IOException;

  /**
   *
   * @param uuid
   */
  public void reRunSubmission(String uuid);

  /**
   *
   * @param entity
   * @param executions
   */
  public void updateExecutions(Submission entity, List<Execution> executions);

}
