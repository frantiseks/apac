/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.monitoring;

import org.apache.http.ParseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Health checker for PlagDetector service. Checker is implemented via
 * {@link AbstractHealthIndicator}.
 *
 * @author František Špaček
 */
@Component
public class PlagDetectorHealthIndicator extends AbstractHealthIndicator {

  private static final Logger LOG = LoggerFactory
      .getLogger(PlagDetectorHealthIndicator.class);

  @Value("${plagdetector.api.status}")
  private String plagMonitorUrl;

  private final CloseableHttpClient client;

  public PlagDetectorHealthIndicator() {
    RequestConfig config = RequestConfig.custom()
        .setSocketTimeout(100)
        .setConnectTimeout(100)
        .build();

    client = HttpClients
        .custom()
        .setDefaultRequestConfig(config)
        .build();
  }

  @Override
  protected void doHealthCheck(Health.Builder builder) throws Exception {
    HttpGet getRequest = new HttpGet(plagMonitorUrl);
    try (CloseableHttpResponse response = client.execute(getRequest)) {
      LOG.info("PlagDetector response status code is {}",
          response.getStatusLine().getStatusCode());
      builder.withDetail("available", true).up();
    } catch (IOException | ParseException ex) {
      LOG.error("unable to get status of plag detector", ex);
      builder.down();
    }
  }
}
