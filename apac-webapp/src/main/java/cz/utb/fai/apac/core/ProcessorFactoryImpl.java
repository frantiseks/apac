/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.core;

import static java.util.Arrays.asList;
import static java.util.ServiceLoader.load;

import cz.utb.fai.apac.commons.enums.Language;
import cz.utb.fai.apac.commons.spi.Processor;
import cz.utb.fai.apac.commons.spi.ProcessorInfo;
import cz.utb.fai.apac.commons.util.GenericExtFilter;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author František Špaček
 */
@Component
public class ProcessorFactoryImpl implements ProcessorFactory {

  private static final Logger LOG = LoggerFactory.getLogger(ProcessorFactoryImpl.class);
  private static ServiceLoader<Processor> processorLoader;
  private static FileAlterationMonitor monitor;
  private static final int MIN_PLUGIN_VERSION = 1;

  @Value("${apac.processors}")
  private String pluginDir;

  public static List<String> getLoadedPlugins() {
    List<String> plugins = new ArrayList<>();

    for (Processor submissionProcessor : processorLoader) {
      plugins.add(submissionProcessor.getClass().getSimpleName());
    }

    return plugins;
  }

  @PostConstruct
  public void init() {
    try {
      File processorsDir = new File(pluginDir);
      loadPlugins(processorsDir);

      //plugin dir changes listener
      FileAlterationObserver directoryObserver = new FileAlterationObserver(processorsDir);
      directoryObserver.addListener(new FileAlterationListenerImpl());

      monitor = new FileAlterationMonitor();
      monitor.addObserver(directoryObserver);

      LOG.info("Starting monitoring plugin directory");
      monitor.start();
    } catch (Exception ex) {
      LOG.error("SubmissionProcessorFactory init exception", ex);
    }
  }

  @PreDestroy
  public void destroy() {
    try {
      LOG.info("Stopping plugin directory monitor.");
      monitor.stop();
    } catch (Exception ex) {
      LOG.error("Plugin Monitor shutdown", ex);
    }
  }

  @Override
  public Optional<Processor> getInstance(String language) {
    Optional<Processor> processor = Optional.empty();
    Language lang = Language.valueOf(language.toUpperCase());
    for (Processor subProcessor : processorLoader) {

      if (subProcessor.getClass().isAnnotationPresent(ProcessorInfo.class)) {
        Annotation annotation = subProcessor
            .getClass()
            .getAnnotation(ProcessorInfo.class);

        ProcessorInfo processorInfo = (ProcessorInfo) annotation;

        if (processorInfo.language() == lang
            && processorInfo.version() >= MIN_PLUGIN_VERSION) {
          processor = Optional.of(subProcessor);
        }
      }
    }

    return processor;
  }

  private void loadPlugins(File processorsDir) {
    //if dir with plugins do not exists create one
    if (!processorsDir.exists()) {
      processorsDir.mkdirs();
    }

    LOG.info("Loading apac plugins from dir {} ",
        processorsDir.getAbsolutePath());

    List<File> jars = asList(processorsDir.listFiles(new GenericExtFilter(new String[]{"jar"})));

    if (jars == null) {
      LOG.info("No apac plugins found");
      return;
    }

    List<URL> jarUrls = new ArrayList<>();
    try {
      for (File f : jars) {
        jarUrls.add(f.toURI().toURL());
      }
    } catch (MalformedURLException ex) {
      LOG.error("Invalid JAR file URL", ex);
    }

    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    //load external class to current runtime
    URLClassLoader jarClassLoader = new URLClassLoader(jarUrls
        .toArray(new URL[jarUrls.size()]), classLoader);

    processorLoader = load(Processor.class, jarClassLoader);

    getLoadedPlugins().forEach(p -> LOG.debug("Plugin loaded {}", p));
  }

  private class FileAlterationListenerImpl implements FileAlterationListener {

    @Override
    public void onStart(FileAlterationObserver fao) {
      LOG.debug("Starting directory monitoring");
    }

    @Override
    public void onDirectoryCreate(File file) {
      LOG.debug("Plugin directory change");
    }

    @Override
    public void onDirectoryChange(File file) {
      LOG.debug("Plugin directory change");
    }

    @Override
    public void onDirectoryDelete(File file) {
      LOG.debug("Plugin directory change");
    }

    @Override
    public void onFileCreate(File file) {
      LOG.debug("New plugin found, reloading");
      loadPlugins(file.getParentFile());
    }

    @Override
    public void onFileChange(File file) {
      LOG.debug("Plugin change was detected");
      loadPlugins(file.getParentFile());
    }

    @Override
    public void onFileDelete(File file) {
      LOG.debug("Plugin has been deleted, reloading");
      loadPlugins(file.getParentFile());
    }

    @Override
    public void onStop(FileAlterationObserver fao) {
      LOG.debug("Stopping directory monitoring");
    }
  }
}
