/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.util;

import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.repository.SubmissionRepository;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author František Špaček
 */
@Component
public class WorkspaceCleaner {

  private static final Logger LOG = LoggerFactory.getLogger(WorkspaceCleaner.class);

  @Value("${apac.cleaner.days}")
  private int days;

  @Value("${apac.temp}")
  private String tempPath;

  @Autowired
  private SubmissionRepository submissionRepository;

  @Scheduled(cron = "0 0 0 15 */1 *")
  public void clean() {
    LOG.info("Workspace cleaner starts");

    LocalDateTime from = LocalDateTime.of(1970, 1, 1, 0, 0);
    LocalDateTime to = LocalDateTime.now().minusDays(days);
    List<Submission> submissions = submissionRepository.findAllBetween(from, to);

    submissions.stream().forEach((s) -> {
      File workspace = WorkspaceUtil.workspace(tempPath, s.getUuid());

      if (workspace.exists()) {
        LOG.debug("Deleting workspace {}", workspace.getAbsolutePath());
        FileUtils.deleteQuietly(workspace);
      }

      s.setVisible(false);
      s.setRunnable(false);
    });

    submissionRepository.save(submissions);
    LOG.info("Workspace cleaner ends");
  }
}
