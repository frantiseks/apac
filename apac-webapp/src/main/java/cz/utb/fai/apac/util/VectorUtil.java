/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.apac.util;

import cz.utb.fai.apac.api.dto.ArgumentDto;
import cz.utb.fai.apac.api.dto.VectorDto;
import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.enums.ArgumentType;

import java.util.List;

/**
 * @author František Špaček
 */
public class VectorUtil {

  public static boolean validate(VectorDto vector) {
    boolean valid = vector.getPoints() > 0;
    valid = valid && vector.getArguments() != null;
    int stdinCounter = 0;

    if (valid && vector.getArguments().isEmpty()) {
      valid = false;
    } else {
      for (ArgumentDto arg : vector.getArguments()) {
        if (arg.getName() == null) {
          arg.setName("");
        }
        valid = valid && arg.getType() != null;
        valid = valid && arg.getValue() != null;

        if (valid && arg.getType().equals(ArgumentType.STDINFILE)) {
          arg.setName("<");
          stdinCounter++;
        }
      }

      valid = valid && stdinCounter < 2;
    }

    return valid;
  }

  public static boolean validateSTDINFile(List<Argument> arguments, String filename) {
    if (arguments == null) {
      return false;
    }

    for (Argument arg : arguments) {
      if (arg.getType().equals(ArgumentType.STDINFILE)) {
        return arg.getValue().equals(filename);
      }
    }

    return true;
  }

  public static String getSTDINFileName(Vector vector) {
    for (Argument arg : vector.getArguments()) {
      if (arg.getType().equals(ArgumentType.STDINFILE)) {
        return arg.getValue();
      }
    }

    return "";
  }
}
