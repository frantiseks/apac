/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.core;

import cz.utb.fai.apac.domain.Assignment;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author František Špaček
 */
public class SubmissionInfo implements Serializable {

  private final long assignmentId;
  private final String uuid;
  private final String language;
  private final boolean reference;
  private String owner;

  public SubmissionInfo(Assignment assignment, String uuid, boolean reference) {
    Objects.requireNonNull(assignment, "Assignment cannot be null");
    this.assignmentId = assignment.getId();
    this.uuid = uuid;
    this.language = assignment.getLang();
    this.reference = reference;
  }

  public String getUuid() {
    return uuid;
  }

  public String getLanguage() {
    return language;
  }

  public long getAssignmentId() {
    return assignmentId;
  }

  public boolean isReference() {
    return reference;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  @Override
  public String toString() {
    return "MessageDTO{" + "uuid=" + uuid + ", language=" + language + '}';
  }
}
