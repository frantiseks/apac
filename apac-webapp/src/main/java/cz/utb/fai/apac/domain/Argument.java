/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.domain;

import cz.utb.fai.apac.enums.ArgumentType;

import javax.persistence.*;

/**
 * @author František Špaček
 */
@Entity
@Table(name = "arguments")
public class Argument extends BaseEntity {

  private String name;

  @Enumerated
  private ArgumentType type;

  @Column(name = "arg_value")
  private String value;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "vector_id")
  private Vector vector;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArgumentType getType() {
    return type;
  }

  public void setType(ArgumentType type) {
    this.type = type;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Vector getVector() {
    return vector;
  }

  public void setVector(Vector vector) {
    this.vector = vector;
  }

  @Override
  public String toString() {
    return "Argument{" + "name=" + name + ", argType=" + type
        + ", argValue=" + value + '}';
  }

}
