/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.service.impl;

import cz.utb.fai.apac.api.dto.filter.SubmissionFilter;
import cz.utb.fai.apac.core.SubmissionProducer;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Execution;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.domain.Submission_;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.exception.AssignmentIncompleteException;
import cz.utb.fai.apac.exception.ResourceNotFoundException;
import cz.utb.fai.apac.exception.UnableToRunException;
import cz.utb.fai.apac.repository.AssignmentRepository;
import cz.utb.fai.apac.repository.ExecutionRepository;
import cz.utb.fai.apac.repository.SubmissionRepository;
import cz.utb.fai.apac.service.SubmissionService;
import cz.utb.fai.apac.util.UUIDUtil;
import cz.utb.fai.apac.util.UploadUtil;
import cz.utb.fai.apac.util.WorkspaceUtil;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.mime.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author František Špaček
 */
@Service
@Transactional
public class SubmissionServiceImpl implements SubmissionService {

  @Autowired
  private SubmissionRepository submissionRepository;

  @Value("${apac.references}")
  private String referenceBasePath;

  @Value("${apac.temp}")
  private String basePath;

  @Autowired
  private AssignmentRepository assignmentRepository;

  @Autowired
  private ExecutionRepository executionRepository;

  @Autowired
  private SubmissionProducer producer;

  @Override
  public Submission createOrUpdate(Submission entity) {
    return submissionRepository.save(entity);
  }

  @Override
  public Submission get(String uuid) {
    Optional<Submission> opSubmission = submissionRepository.findByUuid(uuid);
    return opSubmission.orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public Submission getOrNew(String uuid) {
    Optional<Submission> opSubmission = submissionRepository.findByUuid(uuid);
    return opSubmission.orElse(new Submission());
  }

  @Override
  public void updateExecutions(Submission entity, List<Execution> executions) {
    //delete old
    executionRepository.delete(entity.getExecutions());
    entity.getExecutions().clear();
    submissionRepository.save(entity);

    //save new
    executionRepository.save(executions);
    entity.getExecutions().addAll(executions);
    submissionRepository.save(entity);
  }

  @Override
  public List<Submission> getAllCurrent(Assignment assignment) {
    return submissionRepository.findAllCurrent(assignment.getId(),
        assignment.getValidFrom(),
        assignment.getValidTo());
  }

  @Override
  public List<Submission> getAll(SubmissionFilter filter) {
    return submissionRepository.findAll(FilterSpec
        .searchByFilter(filter == null
            ? new SubmissionFilter()
            : filter));
  }

  @Override
  public void delete(String uuid) {
    Submission submission = get(uuid);
    if (submission.getExecutions().isEmpty()) {
      submissionRepository.delete(submission);
    } else {
      submission.setVisible(false);
      submissionRepository.save(submission);
    }
  }

  @Override
  public String processSubmission(String uuid, File submissionFile) throws IOException {
    Submission submission = get(uuid);
    return processSubmission(submission.getAssignment().getId(), submissionFile, uuid,
        submission.getReference(), submission.getOwner());
  }

  @Override
  public String processSubmission(long assignmentId, File submissionFile, String owner)
      throws IOException {
    return processSubmission(assignmentId, submissionFile, UUIDUtil.random(), false, owner);
  }

  @Override
  public String processSubmission(long assignmentId, File submissionFile, boolean reference)
      throws IOException {
    return processSubmission(assignmentId, submissionFile, UUIDUtil.random(), reference, "");
  }

  @Override
  public String processSubmission(long assignmentId, File submissionFile, String uuid,
      boolean reference, String owner) throws IOException {
    MediaType mediaType = UploadUtil.detectType(submissionFile);
    Optional<Assignment> opAssignment = reference
        ? assignmentRepository.findById(assignmentId)
        : assignmentRepository.findByIdAndVisibleTrue(assignmentId);

    Assignment assignment = opAssignment.orElseThrow(ResourceNotFoundException::new);

    boolean evaluate = !assignment.getVectors().isEmpty();

    if (reference) {
      processReferenceSubmission(assignment, submissionFile, mediaType);
    } else if (!assignment.isCreatedReference() || StringUtils.isEmpty(assignment.getBasefile())) {
      throw new AssignmentIncompleteException(String
          .format("Reference submission for assignment %s not found", assignment.getId()));
    }

    if (mediaType.equals(MediaType.APPLICATION_ZIP)) {
      ZipUtil.unpack(submissionFile, WorkspaceUtil.workspace(basePath, uuid));
    } else {
      FileUtils.copyFileToDirectory(submissionFile, WorkspaceUtil.workspace(basePath, uuid));
    }

    File refFolder = new File(assignment.getBasefile());
    for (Vector v : assignment.getVectors()) {
      if (!StringUtils.isEmpty(v.getFilename())) {
        File vectorFile = new File(refFolder.getAbsolutePath() + File.separator + v.getFilename());
        FileUtils.copyFileToDirectory(vectorFile, WorkspaceUtil.workspace(basePath, uuid));
      }
    }

    if (evaluate) {
      producer.create(assignment, uuid, reference, owner);
    } else {
      throw new AssignmentIncompleteException("Testing vectors for assignment was not defined");
    }

    FileUtils.deleteQuietly(submissionFile);
    return uuid;
  }

  @Override
  public void reRunSubmission(String uuid) {
    Submission submission = get(uuid);

    if (submission.getCompileFailed()) {
      throw new UnableToRunException("Submission has not been sucessfully compiled");
    }

    producer.create(submission.getAssignment(), uuid, submission.getReference());
  }

  @Override
  public void reRunReferenceSubmission(Assignment assignment) throws IOException {
    File referenceFolder = new File(assignment.getBasefile());

    File[] files = referenceFolder.listFiles(File::isFile);

    if (files != null) {
      String uuid = UUIDUtil.random();

      for (File file : files) {
        FileUtils.copyFileToDirectory(file, WorkspaceUtil.workspace(basePath, uuid));
      }
    }
  }

  @Override
  public List<Submission> getAllByAssignmentId(Long assignmentId, boolean reference) {
    return submissionRepository.findAllSubmissions(assignmentId, reference);
  }

  private void processReferenceSubmission(Assignment assignment, File uploadedFile, MediaType type)
      throws IOException {
    File referenceFolder = new File(referenceBasePath + File.separator + assignment.getId());

    if (!referenceFolder.exists()) {
      referenceFolder.mkdirs();
    }

    if (type.equals(MediaType.APPLICATION_ZIP)) {
      ZipUtil.unpack(uploadedFile, referenceFolder);
    } else {
      //copy file to assignments references dir
      FileUtils.copyFileToDirectory(uploadedFile, referenceFolder);
    }
    assignment.setBasefile(referenceFolder.getAbsolutePath());

    List<Submission> referenceSubmission = assignment.getSubmissions().stream()
        .filter(s -> s.getReference())
        .collect(Collectors.toList());

    referenceSubmission.stream().forEach((s) -> {
      s.setVisible(false);
    });

    submissionRepository.save(referenceSubmission);
    assignmentRepository.save(assignment);
  }

  private static class FilterSpec {

    public static Specification<Submission> searchByFilter(SubmissionFilter filter) {
      return (Root<Submission> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
        Predicate predicate = cb.conjunction();
        predicate.getExpressions()
            .add(cb.equal(root.get(Submission_.visible), filter.isVisible()));

        LocalDateTime fromDateTime = LocalDateTime.now().minusMonths(6);
        LocalDateTime toDateTime = LocalDateTime.now();

        if (Objects.nonNull(filter.getFrom())) {
          fromDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(filter.getFrom()),
              ZoneId.systemDefault());
        }
        if (Objects.nonNull(filter.getTo())) {
          toDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(filter.getTo()),
              ZoneId.systemDefault());
        }

        predicate.getExpressions().add(cb.between(root.get(Submission_.timestamp), fromDateTime,
            toDateTime));
        return predicate;
      };
    }
  }
}
