/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.util;

import cz.utb.fai.apac.commons.enums.Extension;
import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.domain.Vector;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author František Špaček
 */
public class CommandUtil {

  /**
   * builds argument list.
   *
   * @param vector
   * @param inputFileDirPath
   * @return
   * @throws IOException
   */
  public static String buildArguments(Vector vector, String inputFileDirPath) throws IOException {
    List<Argument> args = vector.getArguments();
    //sorting to make sure that stdin is last
    Collections.sort(args, (Argument o1, Argument o2) -> o1.getType().compareTo(o2.getType()));

    StringBuilder sb = new StringBuilder();

    for (Argument arg : args) {
      if (!arg.getName().isEmpty()) {
        sb.append(arg.getName().trim());
        sb.append(" ");
      }

      switch (arg.getType()) {
        case INPUTFILE:
          if (StringUtils.isNotBlank(vector.getFilename())) {
            sb.append(vector.getFilename());
            sb.append(" ");
          } else {
            File inputFile = getInputFileName(inputFileDirPath);
            if (inputFile.createNewFile()) {
              FileUtils.writeStringToFile(inputFile, arg.getValue());
              sb.append(inputFile.getName());
              sb.append(" ");
            }
          }
          break;
        case STDINFILE:
          if (StringUtils.isNotBlank(vector.getFilename())) {
            sb.append(vector.getFilename());
            sb.append(" ");
          }
          break;
        case OUTPUTFILE:
          if (!arg.getValue().isEmpty()) {
            sb.append(arg.getValue().trim());
            sb.append(".").append(Extension.OUT);
            sb.append(" ");
          }
          break;
        default:
          if (!arg.getValue().isEmpty()) {
            sb.append(arg.getValue().trim());
            sb.append(" ");
          }
      }
    }

    return sb.toString();
  }

  private static File getInputFileName(String path) {
    return new File(String.format("%s%s%s.%s", path, File.separator, UUIDUtil.random(), 
        Extension.IN));
  }
}
