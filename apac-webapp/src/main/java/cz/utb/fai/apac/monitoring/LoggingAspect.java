/*
 * Copyright (C) 2015 , František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.monitoring;

import com.google.common.base.Throwables;

import cz.utb.fai.apac.exception.AssignmentIncompleteException;
import cz.utb.fai.apac.exception.FilenameCannotBeEmptyException;
import cz.utb.fai.apac.exception.InvalidFileException;
import cz.utb.fai.apac.exception.InvalidVectorException;
import cz.utb.fai.apac.exception.ProcessorNotFoundException;
import cz.utb.fai.apac.exception.ResourceNotFoundException;
import cz.utb.fai.apac.exception.UnableToRunException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Aspect for logging execution of service and repository Spring components.
 */
@Aspect
public class LoggingAspect {

  private final Logger LOG = LoggerFactory.getLogger(LoggingAspect.class);

  @Pointcut("within(cz.utb.fai.apac.repository..*) "
      + "|| within(cz.utb.fai.apac.service..*) "
      + "|| within(cz.utb.fai.apac.core..*) "
      + "|| within(cz.utb.fai.apac.api..*)")
  public void loggingPoincut() {
  }

  @AfterThrowing(pointcut = "loggingPoincut()", throwing = "e")
  public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
    if (e instanceof InvalidFileException
        || e instanceof UnableToRunException
        || e instanceof ProcessorNotFoundException
        || e instanceof FilenameCannotBeEmptyException
        || e instanceof AssignmentIncompleteException
        || e instanceof InvalidVectorException
        || e instanceof ResourceNotFoundException) {
      LOG.info("Validation exception {} - {}", e.getClass(), e.getMessage());
    } else {
      LOG.error("Exception in {}.{}() with cause = {}",
          joinPoint.getSignature().getDeclaringTypeName(),
          joinPoint.getSignature().getName(),
          Throwables.getStackTraceAsString(e));
    }
  }

  @Around("loggingPoincut()")
  public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Enter: {}.{}() with argument[s] = {}",
          joinPoint.getSignature().getDeclaringTypeName(),
          joinPoint.getSignature().getName(),
          Arrays.toString(joinPoint.getArgs()));
    }
    try {
      Object result = joinPoint.proceed();
      if (LOG.isDebugEnabled()) {
        LOG.debug("Exit: {}.{}() with result = {}",
            joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName(), result);
      }
      return result;
    } catch (IllegalArgumentException e) {
      LOG.error("Illegal argument: {} in {}.{}()",
          Arrays.toString(joinPoint.getArgs()),
          joinPoint.getSignature().getDeclaringTypeName(),
          joinPoint.getSignature().getName());

      throw e;
    }
  }
}
