CREATE TABLE arguments 
  ( 
     id        BIGSERIAL NOT NULL, 
     visible   BOOLEAN NOT NULL, 
     name      VARCHAR(255), 
     type      INT4, 
     arg_value VARCHAR(255), 
     vector_id BIGINT NOT NULL, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE assignments 
  ( 
     id                       BIGSERIAL NOT NULL, 
     visible                  BOOLEAN NOT NULL, 
     allowed_compile_warnings INT4 NOT NULL, 
     assignment_plag_id       VARCHAR(255), 
     basefile                 VARCHAR(255), 
     compilation_weight       FLOAT8 NOT NULL, 
     created_reference        BOOLEAN NOT NULL, 
     description              VARCHAR(1000), 
     enable_networking        BOOLEAN NOT NULL, 
     execution_weight         FLOAT8 NOT NULL, 
     lang                     VARCHAR(255) NOT NULL, 
     max_runtime              INT4 NOT NULL, 
     max_size                 INT4 NOT NULL, 
     name                     VARCHAR(255) NOT NULL, 
     threshold                INT4 NOT NULL, 
     valid_from               TIMESTAMP, 
     valid_to                 TIMESTAMP, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE executions 
  ( 
     id            BIGSERIAL NOT NULL, 
     visible       BOOLEAN NOT NULL, 
     log           TEXT, 
     score         FLOAT8 NOT NULL, 
     status_code   INT4, 
     terminated    BOOLEAN NOT NULL, 
     vector        BYTEA, 
     submission_id BIGINT NOT NULL, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE submission_similarities 
  ( 
     id                      BIGSERIAL NOT NULL, 
     visible                 BOOLEAN NOT NULL, 
     similarity              FLOAT8 NOT NULL, 
     base_submission_id      BIGINT NOT NULL, 
     suspected_submission_id BIGINT NOT NULL, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE submissions 
  ( 
     id                 BIGSERIAL NOT NULL, 
     visible            BOOLEAN NOT NULL, 
     compile_failed     BOOLEAN NOT NULL, 
     compile_output     TEXT, 
     owner              VARCHAR(255) NOT NULL, 
     plagiarism_checked BOOLEAN NOT NULL, 
     reference          BOOLEAN, 
     runnable           BOOLEAN NOT NULL, 
     similarity         FLOAT8 NOT NULL, 
     time_stamp         TIMESTAMP NOT NULL, 
     total_points       FLOAT8 NOT NULL, 
     uuid               VARCHAR(255) NOT NULL, 
     assignment_id      BIGINT NOT NULL, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE vectors 
  ( 
     id         BIGSERIAL NOT NULL, 
     visible    BOOLEAN NOT NULL, 
     filename   VARCHAR(255), 
    max_points FLOAT8 NOT NULL, 
     ref_output TEXT, 
     assignment_id BIGINT NOT NULL, 
     PRIMARY KEY (id) 
  ); 

ALTER TABLE submission_similarities 
  ADD CONSTRAINT uk_71o38hje8lxgwac5ylga4idfm UNIQUE (base_submission_id, 
  suspected_submission_id); 

ALTER TABLE arguments 
  ADD CONSTRAINT fk_6p3a3weoh10sw0cxnccwbql53 FOREIGN KEY (vector_id) REFERENCES 
  vectors; 

ALTER TABLE executions 
  ADD CONSTRAINT fk_iyx246dwr7m7o9bxd0ski1uuu FOREIGN KEY (submission_id) 
  REFERENCES submissions; 

ALTER TABLE submission_similarities 
  ADD CONSTRAINT fk_qd5neo8mnpk1psf8o54xld8gp FOREIGN KEY (base_submission_id) 
  REFERENCES submissions; 

ALTER TABLE submission_similarities 
  ADD CONSTRAINT fk_ryavosnv77se9cbr0qpb7a6e1 FOREIGN KEY ( 
  suspected_submission_id) REFERENCES submissions; 

ALTER TABLE submissions 
  ADD CONSTRAINT fk_tmp9uq9lu4lneyua21wexw0pt FOREIGN KEY (assignment_id) 
  REFERENCES assignments; 

ALTER TABLE vectors 
  ADD CONSTRAINT fk_rm6ripd0btrj7k3h50xh0ttaq FOREIGN KEY (assignment_id) 
  REFERENCES assignments; 