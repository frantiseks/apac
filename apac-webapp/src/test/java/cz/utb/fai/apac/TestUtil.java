/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.util.UUIDUtil;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;

/**
 * @author František Špaček
 */
public class TestUtil {

  public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

  public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    return mapper.writeValueAsBytes(object);
  }

  public static String createStringWithLength(int length) {
    StringBuilder builder = new StringBuilder();

    for (int index = 0; index < length; index++) {
      builder.append("a");
    }

    return builder.toString();
  }

  public static Assignment randomAssignment() {
    Assignment assignment = new Assignment();
    assignment.setAllowedCompileWarnings(RandomUtils.nextInt());
    assignment.setAssignmentPlagId(RandomStringUtils.randomAlphanumeric(10));
    assignment.setBasefile("/test/test.zip");
    assignment.setCompilationWeight(RandomUtils.nextDouble());
    assignment.setExecutionWeight(RandomUtils.nextDouble());
    assignment.setCreatedReference(Boolean.TRUE);
    assignment.setDescription(RandomStringUtils.randomAlphanumeric(1000));
    assignment.setLang("C");
    assignment.setMaxRuntime(RandomUtils.nextInt());
    assignment.setMaxSize(RandomUtils.nextInt());
    assignment.setName(RandomStringUtils.randomAlphanumeric(255));
    assignment.setThreshold(RandomUtils.nextInt());
    assignment.setValidFrom(LocalDateTime.now());
    assignment.setValidTo(LocalDateTime.now().plusMonths(6));
    return assignment;
  }

  public static Vector randomVector(Assignment assignment) {
    Vector vector = new Vector();
    vector.setAssignment(assignment);
    vector.setMaxPoints(RandomUtils.nextDouble());
    return vector;
  }

  public static Submission randomSubmission(Assignment assignment) {
    Submission submission = new Submission();
    submission.setOwner(RandomStringUtils.randomAlphanumeric(10));
    submission.setSimilarity(RandomUtils.nextDouble());
    submission.setTimestamp(LocalDateTime.now());
    submission.setTotalPoints(RandomUtils.nextDouble());
    submission.setUuid(UUIDUtil.random());
    submission.setAssignment(assignment);
    submission.setReference(false);
    return submission;
  }
}
