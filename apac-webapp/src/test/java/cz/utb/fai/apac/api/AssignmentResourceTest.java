/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import cz.utb.fai.apac.Application;
import cz.utb.fai.apac.TestUtil;
import cz.utb.fai.apac.api.dto.*;
import cz.utb.fai.apac.api.dto.filter.AssignmentFilter;
import cz.utb.fai.apac.api.dto.filter.SubmissionFilter;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Execution;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.enums.ArgumentType;
import cz.utb.fai.apac.repository.AssignmentRepository;
import cz.utb.fai.apac.repository.ExecutionRepository;
import cz.utb.fai.apac.repository.SubmissionRepository;
import cz.utb.fai.apac.repository.VectorRepository;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author František Špaček
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class AssignmentResourceTest {

  private static final String HELLO_WORLD_IN_C = "#include<stdio.h>\n"
      + "\n"
      + "main()\n"
      + "{\n"
      + "    printf(\"Hello World\");\n"
      + "\n"
      + "}";
  private static final String FORK_IN_C = "#include <unistd.h>\n"
      + " \n"
      + "int main(void)\n"
      + "{\n"
      + "    while(1)\n"
      + "        fork();\n"
      + "}";

  @Autowired
  private WebApplicationContext context;

  private MockMvc mockMvc;
  @Autowired
  private AssignmentRepository assignmentRepositoryMock;

  @Autowired
  private VectorRepository vectorRepositoryMock;

  @Autowired
  private SubmissionRepository submissionRepositoryMock;

  @Autowired
  private ExecutionRepository executionReposityMock;

  @Value("${apac.references}")
  private String referencesPath;

  public AssignmentResourceTest() {
  }

  @Before
  public void setUp() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    new File(referencesPath).mkdir();
  }

  @Test
  public void testCreate() throws Exception {
    AssignmentDto dto = new AssignmentDto();
    String name = RandomStringUtils.randomAlphabetic(255);
    dto.setName(name);
    dto.setDescription(RandomStringUtils.randomAlphabetic(1000));
    dto.setLanguage("C");
    dto.setMaxRuntime(10);
    dto.setMaxSize(1);
    dto.setThreshold(60);
    dto.setAllowedCompileWarnings(1);
    dto.setCompilationWeight(0.5);
    dto.setExecutionWeight(0.5);

    mockMvc.perform(post("/api/assignments")
        .contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(dto)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", notNullValue()))
        .andExpect(jsonPath("$.name", is(name)));

  }

  @Test
  public void testGet() throws Exception {
    Assignment assignment = saveRandomAssignment();

    mockMvc.perform(get("/api/assignments/{id}", assignment.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", notNullValue()))
        .andExpect(jsonPath("$.name", is(assignment.getName())));
  }

  @Test
  public void testUpdate() throws Exception {
    Assignment assignment = saveRandomAssignment();

    AssignmentDto dto = AssignmentDto.fromEntity(assignment);
    dto.setName("UPDATED");

    mockMvc.perform(put("/api/assignments/{id}", dto.getId())
        .contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(dto)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", notNullValue()))
        .andExpect(jsonPath("$.name", is(dto.getName())));
  }

  @Test
  public void testDelete() throws Exception {
    Assignment assignment = saveRandomAssignment();

    mockMvc.perform(delete("/api/assignments/{id}", assignment.getId()))
        .andExpect(status().isOk());

    mockMvc.perform(get("/api/assignments/{id}", assignment.getId()))
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetAll() throws Exception {
    for (int i = 1; i < 10; i++) {
      saveRandomAssignment();
    }

    mockMvc.perform(post("/api/assignments/filter")
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(TestUtil.convertObjectToJsonBytes(new AssignmentFilter())))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8));

  }

  @Test
  public void testUploadReference() throws Exception {
    File tempFile = File.createTempFile("exampleC", "tmp", null);
    FileUtils.writeStringToFile(tempFile, HELLO_WORLD_IN_C);

    Assignment assignment = saveRandomAssignment();
    mockMvc.perform(post("/api/assignments/{id}/upload", assignment.getId())
        .param("filename", "hello.c")
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .content(FileUtils.readFileToByteArray(tempFile)))
        .andExpect(status().isExpectationFailed());

    assignment = assignmentRepositoryMock.findOne(assignment.getId());
    Assert.isNull(assignment.getBasefile());
  }

  @Test
  @Transactional
  public void testAddTestVector() throws Exception {
    Assignment assignment = saveRandomAssignment();

    mockMvc.perform(post("/api/assignments/{id}/vectors", assignment.getId())
        .contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(randomVector(assignment))))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", notNullValue()));

    assignment = assignmentRepositoryMock.findOne(assignment.getId());
    Assert.notEmpty(assignment.getVectors());
  }

  @Test
  public void testGetVectors() throws Exception {
    Assignment assignment = saveRandomAssignment();

    VectorDto dto = randomVector(assignment);
    Vector entity = VectorDto.toEntity(dto);
    entity.setAssignment(assignment);

    vectorRepositoryMock.save(entity);
    assignment.getVectors().add(entity);
    assignmentRepositoryMock.save(assignment);

    mockMvc.perform(get("/api/assignments/{id}/vectors", assignment.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$[0].id", notNullValue()));

  }

  @Test
  @Ignore
  public void testCreateSubmission() throws Exception {
    File tempFile = File.createTempFile("exampleC", "tmp", null);
    FileUtils.writeStringToFile(tempFile, HELLO_WORLD_IN_C);

    Assignment assignment = saveRandomAssignment();

    mockMvc.perform(post("/api/assignments/{id}/vectors", assignment.getId())
        .contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(randomVector(assignment))))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", notNullValue()));

    mockMvc.perform(post("/api/assignments/{id}/upload", assignment.getId())
        .param("filename", "hello.c")
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .content(FileUtils.readFileToByteArray(tempFile)))
        .andExpect(status().isOk());

    TimeUnit.MINUTES.sleep(1);

    assignment = assignmentRepositoryMock.findOne(assignment.getId());
    Assert.notNull(assignment.getBasefile());
    Assert.isTrue(assignment.isCreatedReference());

    mockMvc.perform(post("/api/assignments/{id}/submissions", assignment.getId())
        .param("filename", "hello.c")
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .content(FileUtils.readFileToByteArray(tempFile)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.uuid", notNullValue()));
  }

  @Test
  @Ignore
  public void testCreateForkBomb() throws Exception {
    File tempFile = File.createTempFile("exampleC", "tmp", null);
    FileUtils.writeStringToFile(tempFile, HELLO_WORLD_IN_C);

    File forkFile = File.createTempFile("forkC", "tmp", null);
    FileUtils.writeStringToFile(forkFile, FORK_IN_C);

    Assignment assignment = saveRandomAssignment();

    mockMvc.perform(post("/api/assignments/{id}/vectors", assignment.getId())
        .contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(randomVector(assignment))))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", notNullValue()));

    mockMvc.perform(post("/api/assignments/{id}/upload", assignment.getId())
        .param("filename", "hello.c")
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .content(FileUtils.readFileToByteArray(tempFile)))
        .andExpect(status().isOk());

    TimeUnit.MINUTES.sleep(1);

    assignment = assignmentRepositoryMock.findOne(assignment.getId());
    Assert.notNull(assignment.getBasefile());
    Assert.isTrue(assignment.isCreatedReference());

    String result = mockMvc.perform(post("/api/assignments/{id}/submissions",
        assignment.getId())
        .param("filename", "fork.c")
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .content(FileUtils.readFileToByteArray(forkFile)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.uuid", notNullValue()))
        .andReturn()
        .getResponse()
        .getContentAsString();

    SubmissionDto submissionInfo = new ObjectMapper().readValue(result,
        SubmissionDto.class);
    //sleep to wait for execution in container;
    TimeUnit.SECONDS.sleep(assignment.getMaxRuntime() * 2);

    Optional<Submission> opSubmission = submissionRepositoryMock
        .findByUuid(submissionInfo.getUuid());
    Assert.isTrue(opSubmission.isPresent());

    Submission submission = opSubmission.get();
    Optional<Execution> terminated = executionReposityMock
        .findAllBySubmissionId(submission.getId()).stream()
        .filter(e -> e.getTerminated())
        .findFirst();

    Assert.isTrue(terminated.isPresent());

  }

  @Test
  @Ignore
  public void testGetSubmissions() throws Exception {
    File tempFile = File.createTempFile("exampleC", "tmp", null);
    FileUtils.writeStringToFile(tempFile, HELLO_WORLD_IN_C);

    Assignment assignment = saveRandomAssignment();

    mockMvc.perform(post("/api/assignments/{id}/vectors", assignment.getId())
        .contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(randomVector(assignment))))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", notNullValue()));

    mockMvc.perform(post("/api/assignments/{id}/upload", assignment.getId())
        .param("filename", "hello.c")
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .content(FileUtils.readFileToByteArray(tempFile)))
        .andExpect(status().isOk());

    //sleep to wait for execution in container;
    TimeUnit.SECONDS.sleep(30);

    assignment = assignmentRepositoryMock.findOne(assignment.getId());
    Assert.notNull(assignment.getBasefile());
    Assert.isTrue(assignment.isCreatedReference());

    mockMvc.perform(post("/api/assignments/{id}/submissions", assignment.getId())
        .param("filename", "hello.c")
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .content(FileUtils.readFileToByteArray(tempFile)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.uuid", notNullValue()));

    //sleep to wait for execution in container;
    TimeUnit.SECONDS.sleep(30);
    mockMvc.perform(post("/api/submissions/filter")
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(TestUtil.convertObjectToJsonBytes(new SubmissionFilter())))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$[0].uuid", notNullValue()));

  }

  private Assignment saveRandomAssignment() {
    Assignment assignment = new Assignment();
    assignment.setName(RandomStringUtils.randomAlphabetic(200));
    assignment.setLang("C");
    assignment.setMaxRuntime(30);
    assignment.setMaxSize(1);
    assignment.setThreshold(50);
    assignment.setDescription(RandomStringUtils.randomAlphabetic(100));
    assignment.setAllowedCompileWarnings(1);
    assignment.setCompilationWeight(0.5);
    assignment.setExecutionWeight(0.5);
    assignmentRepositoryMock.save(assignment);

    return assignment;
  }

  private VectorDto randomVector(Assignment assignment) {
    VectorDto dto = new VectorDto();
    dto.setArguments(randomArguments());
    dto.setAssignmentId(assignment.getId());
    dto.setPoints(10);
    return dto;
  }

  private List<ArgumentDto> randomArguments() {

    List<ArgumentDto> dtos = new ArrayList<>();

    ArgumentDto arg1 = new ArgumentDto();
    arg1.setName("a");
    arg1.setValue("1");
    arg1.setType(ArgumentType.LONG);
    dtos.add(arg1);

    ArgumentDto arg2 = new ArgumentDto();
    arg2.setName("d");
    arg2.setValue("1");
    arg2.setType(ArgumentType.INPUTFILE);
    dtos.add(arg2);

    ArgumentDto arg3 = new ArgumentDto();
    arg3.setName("c");
    arg3.setValue("1");
    arg3.setType(ArgumentType.STDINFILE);
    dtos.add(arg3);

    return dtos;

  }

}
