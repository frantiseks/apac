/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.util.FileSystemUtils;

import java.io.File;

import javax.jms.ConnectionFactory;
import javax.jms.MessageListener;

@Configuration
@ComponentScan
@EnableJpaRepositories
@EnableAutoConfiguration
@Profile("test")
public class TestApplication {

  @Autowired
  private ConnectionFactory connectionFactory;

  public static void main(String[] args) {
    FileSystemUtils.deleteRecursively(new File("activemq-data"));
    SpringApplication.run(TestApplication.class, args);

  }

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Bean
  @Autowired
  DefaultMessageListenerContainer messageListener(MessageListener submissionListener) {
    DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
    container.setMessageListener(submissionListener);
    container.setConnectionFactory(connectionFactory);
    container.setDestinationName("submissions");
    container.setCacheLevel(DefaultMessageListenerContainer.CACHE_SESSION);
    container.setSessionTransacted(true);
    return container;
  }
}
