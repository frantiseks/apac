/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.service;

import static org.junit.Assert.*;

import cz.utb.fai.apac.AbstractTest;
import cz.utb.fai.apac.TestUtil;
import cz.utb.fai.apac.api.dto.filter.SubmissionFilter;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.exception.ResourceNotFoundException;
import cz.utb.fai.apac.util.UUIDUtil;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @author František Špaček
 */
public class SubmissionServiceTest extends AbstractTest {

  @Autowired
  private SubmissionService submissionService;

  @Autowired
  private AssignmentService assignmentService;

  @Test
  public void testGet() {
    Submission created = createRandomSubmission();

    Submission fetched = submissionService.get(created.getUuid());
    assertEquals(fetched.getId(), created.getId());
    assertEquals(fetched.getUuid(), created.getUuid());
  }

  @Test
  public void testCreateOrUpdate() {
    Assignment assignment = assignmentService.create(TestUtil
        .randomAssignment());
    Submission created = submissionService.createOrUpdate(TestUtil
        .randomSubmission(assignment));
    assertNotNull(created.getId());

    created.setTotalPoints(100);
    created.setSimilarity(1d);

    Submission updated = submissionService.createOrUpdate(created);
    assertNotNull(updated.getId());
    assertEquals(updated.getTotalPoints(), created.getTotalPoints(), 0);
    assertEquals(updated.getSimilarity(), created.getSimilarity(), 0);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testDelete() {
    Assignment assignment = assignmentService.create(TestUtil
        .randomAssignment());
    Submission created = submissionService.createOrUpdate(TestUtil
        .randomSubmission(assignment));
    assertNotNull(created.getId());

    submissionService.delete(created.getUuid());
    submissionService.get(created.getUuid());
  }

  @Test
  public void testGetAll() {
    Submission oldSubmission = createRandomSubmission();
    oldSubmission.setTimestamp(LocalDateTime.now().minusMonths(1));
    submissionService.createOrUpdate(oldSubmission);

    createRandomSubmission();

    SubmissionFilter filter = new SubmissionFilter();
    assertTrue(submissionService.getAll(filter).size() == 2);

    filter.setFrom(LocalDateTime.now().minusDays(7).toInstant(ZoneOffset.UTC)
        .toEpochMilli());
    filter.setTo(LocalDateTime.now().plusDays(7).toInstant(ZoneOffset.UTC)
        .toEpochMilli());
    assertTrue(submissionService.getAll(filter).size() == 1);

    filter = new SubmissionFilter();
    filter.setVisible(false);
    assertTrue(submissionService.getAll(filter).isEmpty());
  }

  @Test
  public void testGetAllByAssignmentId() {
    Assignment assignment = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(assignment.getId());

    Submission created = submissionService.createOrUpdate(TestUtil
        .randomSubmission(assignment));
    assertNotNull(created.getId());

    Submission referenceSubmission = TestUtil.randomSubmission(assignment);
    referenceSubmission.setReference(Boolean.TRUE);
    created = submissionService.createOrUpdate(referenceSubmission);
    assertNotNull(created.getId());
    assertTrue("Incorrect number of returned objects",
        submissionService.getAllByAssignmentId(assignment.getId(), true)
        .size() == 1);
    assertTrue("Incorrect number of returned objects",
        submissionService.getAllByAssignmentId(assignment.getId(), false)
        .size() == 1);
  }

  @Test
  public void testGetAllCurrent() {
    Assignment randomAssignment = TestUtil.randomAssignment();
    randomAssignment.setValidFrom(LocalDateTime.now().minusMonths(6));
    randomAssignment.setValidTo(LocalDateTime.now().plusMonths(1));
    Assignment assignment = assignmentService.create(randomAssignment);
    assertNotNull(assignment.getId());

    Submission oldSubmission = TestUtil.randomSubmission(assignment);
    oldSubmission.setTimestamp(LocalDateTime.now().minusYears(1));

    Submission created = submissionService.createOrUpdate(oldSubmission);
    assertNotNull(created.getId());
    assertTrue(submissionService.getAllCurrent(assignment).isEmpty());
  }

  @Test
  public void testGetOrNew() {
    Assignment assignment = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(assignment.getId());

    Submission created = submissionService.createOrUpdate(TestUtil
        .randomSubmission(assignment));
    assertNotNull(created.getId());

    Submission fetched = submissionService.getOrNew(created.getUuid());
    assertNotNull(fetched.getId());

    fetched = submissionService.getOrNew(UUIDUtil.random());
    assertNull(fetched.getId());
  }

  @Test
  public void testProcessSubmission_3args_1() throws Exception {
  }

  @Test
  public void testProcessSubmission_3args_2() throws Exception {
  }

  @Test
  public void testProcessSubmission_5args() throws Exception {
  }

  @Test
  public void testProcessSubmission_String_File() throws Exception {
  }

  @Test
  public void testReRunReferenceSubmission() throws Exception {
  }

  @Test
  public void testReRunSubmission() {
  }

  @Test
  public void testUpdateExecutions() {
  }

  private Submission createRandomSubmission() {
    Assignment assignment = assignmentService
        .create(TestUtil.randomAssignment());
    assertNotNull(assignment.getId());

    Submission created = submissionService.createOrUpdate(TestUtil
        .randomSubmission(assignment));
    assertNotNull(created.getId());

    return created;
  }
}
