# APAC - Automatic Programming Assignment Checker #

Main motivation for this project was to create safe runtime environment for execution potentially dangerous (student's) programs. Safe runtime environment (sandbox) was created on operating system Linux with container virtualization platform Docker. Project also aims to management of submissions incoming queue. This queue is continuously processed.

#APAC 2.0#
This version is built on top of Spring Boot and Spring 4 framework. It uses JMS queue for background submission processing.