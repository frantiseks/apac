/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Frantisek Spacek
 */
public class ContainerState {

  @SerializedName("Error")
  private String error;

  @SerializedName("OOMKilled")
  private boolean oomKilled;

  @SerializedName("Paused")
  private boolean paused;

  @SerializedName("Running")
  private boolean running;

  @SerializedName("Restarting")
  private boolean restarting;

  @SerializedName("Pid")
  private int pid;

  @SerializedName("ExitCode")
  private int exitCode;

  @SerializedName("StartedAt")
  private String startedAt;

  @SerializedName("FinishedAt")
  private String finishedAt;

  public boolean isRunning() {
    return running;
  }

  public void setRunning(boolean running) {
    this.running = running;
  }

  public int getPid() {
    return pid;
  }

  public void setPid(int pid) {
    this.pid = pid;
  }

  public int getExitCode() {
    return exitCode;
  }

  public void setExitCode(int exitCode) {
    this.exitCode = exitCode;
  }

  public String getStartedAt() {
    return startedAt;
  }

  public void setStartedAt(String startedAt) {
    this.startedAt = startedAt;
  }

  public String getFinishedAt() {
    return finishedAt;
  }

  public void setFinishedAt(String finishedAt) {
    this.finishedAt = finishedAt;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public boolean isOomKilled() {
    return oomKilled;
  }

  public void setOomKilled(boolean oomKilled) {
    this.oomKilled = oomKilled;
  }

  public boolean isPaused() {
    return paused;
  }

  public void setPaused(boolean paused) {
    this.paused = paused;
  }

  public boolean isRestarting() {
    return restarting;
  }

  public void setRestarting(boolean restarting) {
    this.restarting = restarting;
  }

  @Override
  public String toString() {
    return "ContainerState{" + "error=" + error + ", oomKilled="
        + oomKilled + ", paused=" + paused + ", running="
        + running + ", restarting=" + restarting + ", pid="
        + pid + ", exitCode=" + exitCode + ", startedAt="
        + startedAt + ", finishedAt=" + finishedAt + '}';
  }

}
