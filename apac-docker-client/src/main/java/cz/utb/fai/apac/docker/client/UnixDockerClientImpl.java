/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.client;

import static java.lang.String.format;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import cz.utb.fai.apac.docker.exception.DockerErrorException;
import cz.utb.fai.apac.docker.exception.DockerException;
import cz.utb.fai.apac.docker.exception.DockerNotFoundException;
import cz.utb.fai.apac.docker.exception.InvalidRequestException;
import cz.utb.fai.apac.docker.filter.ListFilter;
import cz.utb.fai.apac.docker.filter.LogsFilter;
import cz.utb.fai.apac.docker.model.Container;
import cz.utb.fai.apac.docker.model.ContainerConfig;
import cz.utb.fai.apac.docker.model.ContainerDetail;
import cz.utb.fai.apac.docker.model.ContainerInfo;
import cz.utb.fai.apac.docker.model.DockerInfo;
import cz.utb.fai.apac.docker.model.DockerVersion;
import cz.utb.fai.apac.docker.model.StartConfig;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;

/**
 * Implementation of {@link DockerClient} over Unix sockets.
 *
 * @author František Špaček
 */
public class UnixDockerClientImpl implements DockerClient {

  /**
   * Instance of logger for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(UnixDockerClientImpl.class);

  private static final int HTTP_PORT = 80;

  private static final String BASE_URL_PATTERN = "unix://localhost:%s/%s";
  private static final String PING_ENDPOINT = "/_ping";
  private static final String INSPECT_ENDPOINT = "/containers/%s/json";
  private static final String CREATE_ENDPOINT = "/containers/create";
  private static final String START_ENDPOINT = "/containers/%s/start";
  private static final String STOP_ENDPOINT = "/containers/%s/stop";
  private static final String KILL_ENDPOINT = "/containers/%s/kill";
  private static final String DELETE_ENDPOINT = "/containers/%s?force=true";
  private static final String LIST_ENDPOINT = "/containers/json";
  private static final String LOGS_ENDPOINT = "/containers/%s/logs";
  private static final String INFO_ENDPOINT = "/info";
  private static final String VERSION_ENDPOINT = "/version";
  private static final String PULL_ENDPOINT = "/images/create?fromImage=%s";

  private final HttpClient httpClient;

  /**
   * Creates client for specified UNIX socket path.
   *
   * @param socketPath path to socket
   * @throws IllegalArgumentException if specified socket path is empty or {@code null}
   * @throws IllegalStateException    if specified socket path do not exists.
   */
  public UnixDockerClientImpl(String socketPath) {
    if (socketPath == null || socketPath.isEmpty()) {
      throw new IllegalArgumentException("Socket path must be non-empty "
          + "string");
    }
    File socket = new File(socketPath);

    if (!socket.exists()) {
      throw new IllegalStateException("Specified docker engine"
          + " socket does not exists");
    }

    Registry<ConnectionSocketFactory> registry = RegistryBuilder
        .<ConnectionSocketFactory>create()
        .register("unix", new UnixSocketFactory(socket))
        .build();

    HttpClientConnectionManager conManager
        = new PoolingHttpClientConnectionManager(registry);

    this.httpClient = HttpClients.custom()
        .setConnectionManager(conManager)
        .build();
  }

  @Override
  public void ping() throws DockerErrorException {
    try {
      URI uri = dockerBaseUri(PING_ENDPOINT).build();
      HttpGet httpGet = new HttpGet(uri);

      LOG.debug("Executing request {}", uri);
      HttpResponse response = httpClient.execute(httpGet);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 200:
          break;
        case 500:
          throw new DockerErrorException("Docker Server Error");
        default:
          throw new DockerErrorException("Unknown Error");
      }
    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to ping docker engine", ex);
    }
  }

  @Override
  public DockerInfo info() throws DockerErrorException {
    try {
      URI uri = dockerBaseUri(INFO_ENDPOINT).build();
      HttpGet httpGet = new HttpGet(uri);

      LOG.debug("Executing request {}", uri);
      HttpResponse response = httpClient.execute(httpGet);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 200:
          break;
        case 500:
          throw new DockerErrorException("Docker Server Error");
        default:
          throw new DockerErrorException("Unknown Error");
      }
      String responseJson = EntityUtils.toString(response.getEntity());
      return new Gson().fromJson(responseJson, DockerInfo.class);

    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to get info about docker engine", ex);
    }
  }

  @Override
  public DockerVersion version() throws DockerErrorException {
    try {
      URI uri = dockerBaseUri(VERSION_ENDPOINT).build();
      HttpGet httpGet = new HttpGet(uri);

      LOG.debug("Executing request {}", uri);
      HttpResponse response = httpClient.execute(httpGet);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 200:
          break;
        case 500:
          throw new DockerErrorException("Docker Server Error");
        default:
          throw new DockerErrorException("Unknown Error");
      }
      String responseJson = EntityUtils.toString(response.getEntity());
      return new Gson().fromJson(responseJson, DockerVersion.class);

    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to get version of docker engine", ex);
    }
  }

  @Override
  public List<Container> get(ListFilter filter)
      throws InvalidRequestException, DockerErrorException {
    StringBuilder filters = new StringBuilder();

    if (filter.getExitedCode() != null) {
      filters.append("exited=").append(filter.getExitedCode())
          .append(";");
      filters.append("status=").append(filter.getStatus())
          .append(";");
    }

    String limit = filter.getLimit() == null
        ? ""
        : String.valueOf(filter.getLimit());

    try {
      URI uri = dockerBaseUri(LIST_ENDPOINT)
          .addParameter("all", String.valueOf(filter.getAll()))
          .addParameter("limit", limit)
          .addParameter("since", filter.getSinceId())
          .addParameter("before", filter.getBeforeId())
          .addParameter("size", String.valueOf(filter.showSize()))
          .addParameter("filters", filters.toString())
          .build();
      HttpGet httpGet = new HttpGet(uri);

      LOG.debug("Executing request {}", uri);
      HttpResponse response = httpClient.execute(httpGet);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 200:
          break;
        case 400:
          throw new InvalidRequestException("Invalid request filter");
        default:
          throw new DockerErrorException("Unknown docker error");
      }

      Type type = new TypeToken<Collection<Container>>() {
      }.getType();

      String responseJson = EntityUtils.toString(response.getEntity());
      LOG.debug("Fetched json response {}", responseJson);

      return new Gson().fromJson(responseJson, type);

    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to get container list", ex);
    }
  }

  @Override
  public ContainerDetail inspect(String id) throws DockerNotFoundException,
      DockerErrorException {
    try {
      URI uri = dockerBaseUri(String.format(INSPECT_ENDPOINT, id))
          .build();
      HttpGet httpGet = new HttpGet(uri);

      LOG.debug("Executing request {}", uri);
      HttpResponse response = httpClient.execute(httpGet);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 200:
          break;
        case 404:
          throw new DockerNotFoundException(format("No such container"
              + " %s", id));
        default:
          throw new DockerErrorException("Docker Server Error");
      }

      String responseJson = EntityUtils.toString(response.getEntity());
      LOG.debug("Fetched json response {}", responseJson);

      return new Gson().fromJson(responseJson, ContainerDetail.class);
    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to get container list", ex);
    }
  }

  @Override
  public ContainerInfo create(ContainerConfig config)
      throws InvalidRequestException, DockerErrorException {
    try {
      URI uri = dockerBaseUri(CREATE_ENDPOINT).build();

      HttpResponse response = postJsonRequest(uri, config);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 201:
          break;
        case 400:
          throw new InvalidRequestException("Invalid container "
              + "configuration");
        case 404:
          throw new InvalidRequestException("Container image not "
              + "found");
        default:
          throw new DockerException("Docker Server Error");
      }

      String responseJson = EntityUtils.toString(response.getEntity());
      LOG.debug("Fetched json response {}", responseJson);

      return new Gson().fromJson(responseJson, ContainerInfo.class);

    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to create container", ex);
    }
  }

  @Override
  public void start(String id, StartConfig config)
      throws DockerNotFoundException, InvalidRequestException,
      DockerErrorException {
    try {
      URI uri = dockerBaseUri(String.format(START_ENDPOINT, id)).build();
      HttpResponse response = postJsonRequest(uri, config);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 204:
          break;
        case 404:
          throw new DockerNotFoundException(format("No such "
              + "container %s", id));
        default:
          throw new DockerException("Docker Server Error");
      }
    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to start container", ex);
    }
  }

  @Override
  public void kill(String id) throws DockerNotFoundException,
      DockerErrorException {
    try {
      URI uri = dockerBaseUri(String.format(KILL_ENDPOINT, id))
          .build();

      HttpResponse response = postJsonRequest(uri);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 204:
          break;
        case 404:
          throw new DockerNotFoundException(format("No such container"
              + " %s", id));
        default:
          throw new DockerException("Docker Server Error");
      }
    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to start container", ex);
    }
  }

  @Override
  public void stop(String id) throws DockerNotFoundException,
      DockerErrorException {
    try {
      URI uri = dockerBaseUri(String.format(STOP_ENDPOINT, id))
          .build();

      HttpResponse response = postJsonRequest(uri);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 204:
          break;
        case 404:
          throw new DockerNotFoundException(format("No such container"
              + " %s", id));
        default:
          throw new DockerException("Docker Server Error");
      }
    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to start container", ex);
    }
  }

  @Override
  public void delete(String id) throws DockerNotFoundException,
      DockerErrorException {
    try {
      URI uri = dockerBaseUri(String.format(DELETE_ENDPOINT, id))
          .build();
      HttpDelete httpDelete = new HttpDelete(uri);
      httpDelete.addHeader("Content-Type",
          ContentType.APPLICATION_JSON.toString());

      LOG.debug("Executing request {}", uri);
      HttpResponse response = httpClient.execute(httpDelete);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 204:
          break;
        case 404:
          throw new DockerNotFoundException(format("No such "
              + "container %s", id));
        default:
          throw new DockerErrorException("Docker Server Error");
      }
    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException("Unable to delete container", ex);
    }
  }

  @Override
  public InputStream getLogs(String id, LogsFilter filter)
      throws DockerNotFoundException, InvalidRequestException,
      DockerErrorException {
    String tail = filter.getTail() == null
        ? "all"
        : String.valueOf(filter.getTail());
    try {
      URI uri = dockerBaseUri(format(LOGS_ENDPOINT, id))
          .addParameter("follow", String.valueOf(filter.getFollow()))
          .addParameter("stdout", String.valueOf(filter.getStdOut()))
          .addParameter("stderr", String.valueOf(filter.getStdErr()))
          .addParameter("since", String.valueOf(filter.getSince()))
          .addParameter("timestamps", String
              .valueOf(filter.getTimestamps()))
          .addParameter("tail", tail)
          .build();

      HttpGet httpGet = new HttpGet(uri);

      LOG.debug("Executing request {}", uri);
      HttpResponse response = httpClient.execute(httpGet);

      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 101:
        case 200:
          break;
        case 404:
          throw new DockerNotFoundException(format("No such "
              + "container %s", id));
        default:
          throw new DockerException("Docker Server Error");
      }
      return response.getEntity().getContent();

    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException(format("Unable to get logs for "
          + "%s", id), ex);
    }
  }

  @Override
  public void pull(String image) throws DockerNotFoundException, DockerErrorException {
    try {
      URI uri = dockerBaseUri(String.format(PULL_ENDPOINT, image))
          .build();
      final HttpResponse response = postJsonRequest(uri);
      int statusCode = response.getStatusLine().getStatusCode();
      LOG.debug("Returned status code {}", statusCode);

      switch (statusCode) {
        case 101:
        case 200:
          break;
        case 404:
          throw new DockerNotFoundException(format("No such "
              + "image %s", image));
        default:
          throw new DockerException("Docker Server Error");
      }
    } catch (URISyntaxException | IOException ex) {
      throw new DockerErrorException(format("Unable to pull image "
          + "%s", image), ex);
    }
  }

  private URIBuilder dockerBaseUri(String path) throws URISyntaxException {
    return new URIBuilder(format(BASE_URL_PATTERN, HTTP_PORT, path));
  }

  private HttpResponse postJsonRequest(URI uri) throws IOException {
    return postJsonRequest(uri, null);
  }

  private HttpResponse postJsonRequest(URI uri, Object payload)
      throws IOException {
    HttpPost httpPost = new HttpPost(uri);
    httpPost.addHeader("Content-Type", ContentType.APPLICATION_JSON
        .toString());

    if (payload != null) {
      String jsonPayload = new Gson().toJson(payload);
      LOG.debug("Request payload: {}", jsonPayload);

      HttpEntity entity = new StringEntity(jsonPayload);
      httpPost.setEntity(entity);
    }

    LOG.debug("Executing request {}", uri);
    return httpClient.execute(httpPost);
  }
}
