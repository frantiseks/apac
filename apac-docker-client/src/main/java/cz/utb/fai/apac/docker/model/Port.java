/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Frantisek Spacek
 */
public class Port {

  @SerializedName("PrivatePort")
  private int privatePort;

  @SerializedName("PublicPort")
  private int publicPort;

  @SerializedName("Type")
  private String type;

  @SerializedName("IP")
  private String ip;

  public int getPrivatePort() {
    return privatePort;
  }

  public void setPrivatePort(int privatePort) {
    this.privatePort = privatePort;
  }

  public int getPublicPort() {
    return publicPort;
  }

  public void setPublicPort(int publicPort) {
    this.publicPort = publicPort;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  @Override
  public String toString() {
    return "Port{" + "privatePort=" + privatePort + ", publicPort=" + publicPort + ", type=" + type + ", ip=" + ip + '}';
  }
}
