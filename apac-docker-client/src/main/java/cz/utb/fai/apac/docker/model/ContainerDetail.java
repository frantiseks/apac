/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.Map;

/**
 * @author Frantisek Spacek
 */
public class ContainerDetail {

  @SerializedName("Id")
  private String id;

  @SerializedName("Created")
  private String created;

  @SerializedName("Path")
  private String path;

  @SerializedName("Args")
  private String[] args;

  @SerializedName("Config")
  private ContainerConfig config;

  @SerializedName("State")
  private ContainerState state;

  @SerializedName("Image")
  private String image;

  @SerializedName("NetworkSettings")
  private NetworkSettings networkSettings;

  @SerializedName("SysInitPath")
  private String sysInitPath;

  @SerializedName("ResolvConfPath")
  private String resolvConfPath;

  @SerializedName("Volumes")
  private Map<String, String> volumes;

  @SerializedName("VolumesRW")
  private Map<String, String> volumesRW;

  @SerializedName("Domainname")
  private String domainName;

  @SerializedName("HostnamePath")
  private String hostnamePath;

  @SerializedName("HostsPath")
  private String hostsPath;

  @SerializedName("Name")
  private String name;

  @SerializedName("Driver")
  private String driver;

  @SerializedName("HostConfig")
  private HostConfig hostConfig;

  @SerializedName("Ulimits")
  private List<Ulimit> ulimits = new ArrayList<>();

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String[] getArgs() {
    return args;
  }

  public void setArgs(String[] args) {
    this.args = args;
  }

  public ContainerConfig getConfig() {
    return config;
  }

  public void setConfig(ContainerConfig config) {
    this.config = config;
  }

  public ContainerState getState() {
    return state;
  }

  public void setState(ContainerState state) {
    this.state = state;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public NetworkSettings getNetworkSettings() {
    return networkSettings;
  }

  public void setNetworkSettings(NetworkSettings networkSettings) {
    this.networkSettings = networkSettings;
  }

  public String getSysInitPath() {
    return sysInitPath;
  }

  public void setSysInitPath(String sysInitPath) {
    this.sysInitPath = sysInitPath;
  }

  public String getResolvConfPath() {
    return resolvConfPath;
  }

  public void setResolvConfPath(String resolvConfPath) {
    this.resolvConfPath = resolvConfPath;
  }

  public Map<String, String> getVolumes() {
    return volumes;
  }

  public void setVolumes(Map<String, String> volumes) {
    this.volumes = volumes;
  }

  public Map<String, String> getVolumesRW() {
    return volumesRW;
  }

  public void setVolumesRW(Map<String, String> volumesRW) {
    this.volumesRW = volumesRW;
  }

  public String getDomainName() {
    return domainName;
  }

  public void setDomainName(String domainName) {
    this.domainName = domainName;
  }

  public String getHostnamePath() {
    return hostnamePath;
  }

  public void setHostnamePath(String hostnamePath) {
    this.hostnamePath = hostnamePath;
  }

  public String getHostsPath() {
    return hostsPath;
  }

  public void setHostsPath(String hostsPath) {
    this.hostsPath = hostsPath;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDriver() {
    return driver;
  }

  public void setDriver(String driver) {
    this.driver = driver;
  }

  public HostConfig getHostConfig() {
    return hostConfig;
  }

  public void setHostConfig(HostConfig hostConfig) {
    this.hostConfig = hostConfig;
  }

  public List<Ulimit> getUlimits() {
    return ulimits;
  }

  public void setUlimits(List<Ulimit> ulimits) {
    this.ulimits = ulimits;
  }

  @Override
  public String toString() {
    return "ContainerDetail{" + "id=" + id + ", created=" + created + ", path="
        + path + ", args=" + Arrays.toString(args) + ", config=" + config
        + ", state=" + state + ", image=" + image + ", networkSettings="
        + networkSettings + ", sysInitPath=" + sysInitPath + ", resolvConfPath="
        + resolvConfPath + ", volumes=" + volumes + ", volumesRW=" + volumesRW
        + ", domainName=" + domainName + ", hostnamePath=" + hostnamePath + ", hostsPath="
        + hostsPath + ", name=" + name + ", driver=" + driver + ", hostConfig=" + hostConfig + '}';
  }
}
