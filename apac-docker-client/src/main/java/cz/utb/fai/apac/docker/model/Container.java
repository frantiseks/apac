/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

/**
 * @author Frantisek Spacek
 */
public class Container {

  @SerializedName("Id")
  private String id;

  @SerializedName("Command")
  private String command;

  @SerializedName("Image")
  private String image;

  @SerializedName("Created")
  private long created;

  @SerializedName("Status")
  private String status;

  @SerializedName("Ports")
  private Port[] ports;   //Example value "49164->6900, 49165->7100"

  @SerializedName("SizeRw")
  private int size;

  @SerializedName("SizeRootFs")
  private int sizeRootFs;

  @SerializedName("Names")
  private String[] names;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCommand() {
    return command;
  }

  public void setCommand(String command) {
    this.command = command;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public long getCreated() {
    return created;
  }

  public void setCreated(long created) {
    this.created = created;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Port[] getPorts() {
    return ports;
  }

  public void setPorts(Port[] ports) {
    this.ports = ports;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public int getSizeRootFs() {
    return sizeRootFs;
  }

  public void setSizeRootFs(int sizeRootFs) {
    this.sizeRootFs = sizeRootFs;
  }

  public String[] getNames() {
    return names;
  }

  public void setNames(String[] names) {
    this.names = names;
  }

  @Override
  public String toString() {
    return "Container{" + "id=" + id + ", command=" + command + ", image=" + image
        + ", created=" + created + ", status=" + status + ", ports=" + ports
        + ", size=" + size + ", sizeRootFs=" + sizeRootFs + ", names=" + Arrays.toString(names) + '}';
  }

}
