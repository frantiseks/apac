/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.filter.impl;

import cz.utb.fai.apac.docker.filter.LogsFilter;

/**
 *
 * @author František Špaček
 */
public class LogsFilterImpl implements LogsFilter {

  private final boolean follow;
  private final boolean stdout;
  private final boolean stderr;
  private final boolean timestamps;
  private final long since;
  private final Integer tail;

  public LogsFilterImpl(boolean follow, boolean stdout, boolean stderr,
      boolean timestamps, long since, Integer tail) {
    this.follow = follow;
    this.stdout = stdout;
    this.stderr = stderr;
    this.timestamps = timestamps;
    this.since = since;
    this.tail = tail;
  }

  @Override
  public boolean getFollow() {
    return follow;
  }

  @Override
  public boolean getStdOut() {
    return stdout;
  }

  @Override
  public boolean getStdErr() {
    return stderr;
  }

  @Override
  public long getSince() {
    return since;
  }

  @Override
  public boolean getTimestamps() {
    return timestamps;
  }

  @Override
  public Integer getTail() {
    return tail;
  }

}
