/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.model;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author expi
 */
public class Ulimit {

  @SerializedName("Hard")
  private int hard;

  @SerializedName("Soft")
  private int soft;

  @SerializedName("Name")
  private String name;

  public Ulimit(int soft, int hard, String name) {
    this.hard = hard;
    this.soft = soft;
    this.name = name;
  }

  public Ulimit() {
  }

  public int getHard() {
    return hard;
  }

  public void setHard(int hard) {
    this.hard = hard;
  }

  public int getSoft() {
    return soft;
  }

  public void setSoft(int soft) {
    this.soft = soft;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Ulimits{" + "hard=" + hard + ", soft=" + soft + ", name=" + name + '}';
  }
}
