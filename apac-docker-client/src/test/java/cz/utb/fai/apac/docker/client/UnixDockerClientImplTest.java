/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cz.utb.fai.apac.docker.exception.DockerErrorException;
import cz.utb.fai.apac.docker.exception.DockerNotFoundException;
import cz.utb.fai.apac.docker.filter.impl.LogsFilterBuilder;
import cz.utb.fai.apac.docker.model.ContainerConfig;
import cz.utb.fai.apac.docker.model.ContainerDetail;
import cz.utb.fai.apac.docker.model.ContainerInfo;
import cz.utb.fai.apac.docker.model.DockerInfo;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Tests for {@link UnixDockerClientImpl}.
 *
 * @author František Špaček
 */
public class UnixDockerClientImplTest {

  private static final String TEST_IMAGE = "busybox";
  private final List<String> containerIds = new ArrayList<>();

  private DockerClient client;

  @Before
  public void setUp() {
    client = new UnixDockerClientImpl("/var/run/docker.sock");
    client.pull(TEST_IMAGE);
  }

  @After
  public void tearDown() {
    containerIds.stream().forEach((id) -> {
      try {
        ContainerDetail container = client.inspect(id);

        if (container.getState().isRunning()) {
          client.kill(id);
        }

        client.delete(id);
      } catch (DockerErrorException ex) {
        System.out.println(ex);
      }
    });
  }

  @Test
  public void testPing() throws Exception {
    System.out.println("testPing");
    client.ping();
  }

  @Test
  public void testInfo() throws Exception {
    System.out.println("info");
    DockerInfo info = client.info();
    assertNotNull(info);
  }

  @Test
  public void testGet() {
    System.out.println("testGet");

    ContainerConfig config = new ContainerConfig();
    config.setImage(TEST_IMAGE);
    ContainerInfo info = client.create(config);
    assertNotNull(info);
    assertNotNull(info.getId());

    containerIds.add(info.getId());
    client.start(info.getId(), null);
    ContainerDetail detail = client.inspect(info.getId());
    assertTrue(detail.getState().isRunning());
  }

  @Test
  public void testInspect() {
    System.out.println("testInspect");
    ContainerConfig config = new ContainerConfig();
    config.setImage(TEST_IMAGE);
    ContainerInfo info = client.create(config);
    assertNotNull(info);
    assertNotNull(info.getId());

    String id = info.getId();
    containerIds.add(id);

    client.start(id, null);

    ContainerDetail detail = client.inspect(id);
    assertFalse(detail.getId().isEmpty());
    assertFalse(detail.getCreated().isEmpty());
  }

  @Test
  public void testCreate() throws Exception {
    System.out.println("testCreate");
    ContainerConfig config = new ContainerConfig();
    config.setImage(TEST_IMAGE);
    ContainerInfo info = client.create(config);
    Assert.assertNotNull(info);
    Assert.assertNotNull(info.getId());

    containerIds.add(info.getId());
  }

  @Test
  public void testStart() throws Exception {
    System.out.println("testStart");
    ContainerConfig config = new ContainerConfig();
    config.setImage(TEST_IMAGE);
    ContainerInfo info = client.create(config);
    Assert.assertNotNull(info);
    Assert.assertNotNull(info.getId());

    String id = info.getId();
    containerIds.add(id);

    client.start(id, null);

    ContainerDetail detail = client.inspect(id);
    Assert.assertTrue(detail.getState().isRunning());
  }

  @Test
  public void testStop() throws Exception {
    System.out.println("testStop");
    ContainerConfig config = new ContainerConfig();
    config.setImage(TEST_IMAGE);
    ContainerInfo info = client.create(config);
    Assert.assertNotNull(info);
    Assert.assertNotNull(info.getId());

    String id = info.getId();
    containerIds.add(id);

    client.start(id, null);
    ContainerDetail detail = client.inspect(id);
    Assert.assertFalse(detail.getState().getStartedAt().isEmpty());

    client.stop(id);
    detail = client.inspect(id);
    Assert.assertFalse(detail.getState().isRunning());
  }

  @Test(expected = DockerNotFoundException.class)
  public void testDelete() throws Exception {
    System.out.println("testDelete");
    ContainerConfig config = new ContainerConfig.Builder()
        .setImage(TEST_IMAGE)
        .build();

    ContainerInfo info = client.create(config);
    Assert.assertNotNull(info);
    Assert.assertNotNull(info.getId());

    String id = info.getId();
    client.delete(id);

    //throws DockerNotFoundException
    client.inspect(id);
  }

  @Test
  public void testKill() {
    System.out.println("testKill");
    ContainerConfig config = new ContainerConfig();
    config.setImage(TEST_IMAGE);
    ContainerInfo info = client.create(config);
    Assert.assertNotNull(info);
    Assert.assertNotNull(info.getId());

    String id = info.getId();
    containerIds.add(id);

    client.start(id, null);

    ContainerDetail detail = client.inspect(id);
    Assert.assertTrue(detail.getState().isRunning());

    client.kill(id);
  }

  @Test
  public void testGetLogs() {
    System.out.println("testGetLogs");
    ContainerConfig config = new ContainerConfig();
    config.setImage(TEST_IMAGE);
    config.setCmd(new String[]{"date"});
    ContainerInfo info = client.create(config);
    Assert.assertNotNull(info);
    Assert.assertNotNull(info.getId());

    String id = info.getId();
    containerIds.add(id);

    client.start(id, null);
    InputStream logStream = client.getLogs(id,
        new LogsFilterBuilder()
        .setStdout(true)
        .setStderr(true)
        .build());

    StringBuilder outputBuilder = new StringBuilder();

    Scanner scanner = new Scanner(logStream);
    while (scanner.hasNextLine()) {
      outputBuilder.append(scanner.nextLine());
    }

    assertFalse(outputBuilder.toString().isEmpty());
  }

}
